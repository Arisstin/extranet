import { Component, OnInit } from '@angular/core';
import { Taxes } from './taxes.classes';
import { GeneralServices } from '../general.services';
import {Notify} from '../../widgets/notification/notification.classes';

@Component({
  selector: 'taxes',
  templateUrl: './taxes.component.html',
  styleUrls: ['./taxes.component.css']
})
export class TaxesComponent implements OnInit {
  loaded = false;
  taxes: Taxes;
  notificate: any;
  constructor(private generalservices: GeneralServices) {
  }
  ngOnInit(): void {
    this.generalservices.getTax().subscribe(res => {
      this.taxes = res;
      this.loaded = true;
    }, err => {
      console.log('err', err);
      if (err.status === 404) {
        this.taxes = new Taxes();
      }
      this.loaded = true;
    });
  }
  onSubmit() {
    console.log(this.taxes);
    this.generalservices.postTax(this.taxes).subscribe(res => {
      this.notificate = new Notify('post', 'success');
    }, err => {
      this.notificate = new Notify('post', 'error');
    });
  }
}
