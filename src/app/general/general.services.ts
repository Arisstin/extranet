import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { BaseService } from '../core/base.service';
import {Contact, ContactForm} from './contacts/contacts.classes';
import {PoliciesPost} from './policies/policies.classes';
import {Taxes} from './taxes/taxes.classes';
import {Payments} from './payment/payment.classes';
import {Landmark} from './landmarks/landmarks.classes';

@Injectable()
export class GeneralServices extends BaseService {
  getContacts(): Observable<Contact[]> {
    return this.get('hotel/contacts');
  }
  addContact(value: ContactForm): Observable<any> {
    return this.post('hotel/contacts/add', value);
  }
  updateContact(value: ContactForm, id: number): Observable<any> {
    return this.post('hotel/contacts/' + id + '/update', value);
  }
  getPolicies(): Observable<PoliciesPost> {
    return this.get('hotel/policy');
  }
  updatePolicies(value: PoliciesPost): Observable<any> {
    return this.post('hotel/policy/update', value);
  }
  getCards(): Observable<any> {
    return this.get('cards');
  }
  getTax(): Observable<any> {
    return this.get('hotel/payments');
  }
  postTax(value: Taxes) {
    return this.post('hotel/payment/tax', value);
  }
  postPayment(value: Payments): Observable<any> {
    return this.post('hotel/payment/methods', value);
  }
  getLandmark(): Observable<Landmark[]> {
    return this.get('hotel/landmarks');
  }
  postLandmark(value: Landmark): Observable<any> {
    return this.post('hotel/landmarks', value);
  }
  updateLandmark(value: Landmark): Observable<any> {
    return this.post('hotel/landmarks/' + value.id + '/update', value);
  }
  removeLandmark(id: number): Observable<any> {
    return this.post('hotel/landmarks/' + id + '/remove', null);
  }
}
