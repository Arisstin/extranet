import { Component, OnInit } from '@angular/core';
import {GeneralServices} from '../general.services';
import {Landmark} from './landmarks.classes';
import {Notify} from '../../widgets/notification/notification.classes';

@Component({
  selector: 'landmarks',
  templateUrl: './landmarks.component.html',
  styleUrls: ['./landmarks.component.css']
})
export class LandmarksComponent implements OnInit {
  notificate: any;
  loaded = false;
  land: string;
  shop: string;
  shopsMarks: Landmark[];
  shopsMarksTypes: any[];
  shopsMarksTranslate: string[];
  landMarks: Landmark[];
  landMarksTypes: any[];
  landMarksTranslate: string[];
  constructor(private generalservices: GeneralServices) {
    this.land = 'OCEAN';
    this.shop = 'SUPERMARKET';
    this.landMarksTypes = [
      {id: 0, title: 'OCEAN'},
      {id: 1, title: 'SEA'},
      {id: 2, title: 'LAKE'},
      {id: 3, title: 'RIVER'},
      {id: 4, title: 'BEACH'},
      {id: 5, title: 'MOUNTAIN'},
      {id: 6, title: 'SKIELEVATOR'},
      {id: 7, title: 'LIONS'}];
    this.landMarksTranslate = ['Океан', 'Море', 'Озеро', 'Река', 'Пляж', 'Гора', 'Горный подъемник', 'Значимые места'];
    this.shopsMarksTypes = [
      {id: 0, title: 'SUPERMARKET'},
      {id: 1, title: 'MARKET'},
      {id: 2, title: 'BAR'},
      {id: 3, title: 'CAFE'},
      {id: 4, title: 'RESTAURANT'}];
    this.shopsMarksTranslate = ['Супермаркет', 'Рынок', 'Бар', 'Кафе', 'Ресторан'];
  }
  ngOnInit(): void {
    this.generalservices.getLandmark().subscribe(res => {
      this.shopsMarks = [];
      this.landMarks = [];
      res.forEach(item => {
        if (this.landMarksTypes.find(x => x.title === item.type)) {
          this.landMarks.push(item);
        } else {
          this.shopsMarks.push(item);
        }
      });
      this.loaded = true;
    }, err => {
      if (err.status === 404) {
        this.shopsMarks = [];
        this.landMarks = [];
      }
      this.loaded = true;
    });
  }
  addShopMark() {
    this.shopsMarks.push(new Landmark(this.shop));
    console.log(this.shopsMarks);
  }
  addLandMark() {
    this.landMarks.push(new Landmark(this.land));
  }
  getTranslate(title: string, mod: string) {
    if (mod === 's') {
      return this.shopsMarksTranslate[this.shopsMarksTypes.findIndex(i => i.title === title)];
    } else {
      return this.landMarksTranslate[this.landMarksTypes.findIndex(i => i.title === title)];
    }
  }
  onSubmitMarks() {
    this.landMarks.forEach((item, i) => {
      if (item.distance <= 0 || item.distance === undefined || item.title === '') {
        if (item.id) {
          this.generalservices.removeLandmark(item.id).subscribe();
        }
        this.landMarks.splice(i, 1);
      } else {
        if (item.id) {
          this.generalservices.updateLandmark(item).subscribe(res => {this.notificate = new Notify('post', 'success'); }, err => {this.notificate = new Notify('post', 'error'); });
        } else {
          this.generalservices.postLandmark(item).subscribe(res => {this.notificate = new Notify('post', 'success'); }, err => {this.notificate = new Notify('post', 'error'); });
        }
      }
    });
  }
  onSubmitShops() {
    this.shopsMarks.forEach((item, i) => {
      if (item.distance === 0 || item.title === '') {
        this.shopsMarks.splice(i, 1);
        if (item.id) {
          this.generalservices.removeLandmark(item.id).subscribe();
        }
      } else {
        if (item.id) {
          this.generalservices.updateLandmark(item).subscribe(res => {this.notificate = new Notify('post', 'success'); }, err => {this.notificate = new Notify('post', 'error'); });
        } else {
          this.generalservices.postLandmark(item).subscribe(res => {this.notificate = new Notify('post', 'success'); }, err => {this.notificate = new Notify('post', 'error'); });
        }
      }
    });
  }
  removeShop(i: number) {
    if (this.shopsMarks[i].id) {
      this.generalservices.removeLandmark(this.shopsMarks[i].id).subscribe();
    }
    this.shopsMarks.splice(i, 1);
  }
  removeLand(i: number) {
    if (this.landMarks[i].id) {
      this.generalservices.removeLandmark(this.landMarks[i].id).subscribe();
    }
    this.landMarks.splice(i, 1);
  }
}
