export class Landmark {
  id: number;
  type: string;
  unit: string;
  distance: number;
  title: string;
  constructor(type: string) {
    this.type = type;
    this.unit = 'm';
    this.distance = 0;
    this.title = '';
  }
}
