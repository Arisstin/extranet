import { Component, OnInit } from '@angular/core';
import {Policies, PoliciesPost, Add} from './policies.classes';
import {GeneralServices} from '../general.services';
import {Notify} from '../../widgets/notification/notification.classes';

@Component({
  selector: 'policies',
  templateUrl: './policies.component.html',
  styleUrls: ['./policies.component.css']
})
export class PoliciesComponent implements OnInit {
  notificate: any;
  loaded = false;
  policies: Policies;
  policiesInfo: PoliciesPost;
  ageArray = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18];
  constructor(private generalservices: GeneralServices) {
    this.generalservices.getPolicies().subscribe(res => {
      this.policies = new Policies(6);
      this.policiesInfo = res;
      if (this.policiesInfo.arrival) {
        const timearr = this.policiesInfo.arrival.split(':');
        this.policies.timestart.setHours(Number(timearr[0]), Number(timearr[1]), Number(timearr[2]));
      }
      if (this.policiesInfo.departure) {
        const timearr = this.policiesInfo.departure.split(':');
        this.policies.timeend.setHours(Number(timearr[0]), Number(timearr[1]), Number(timearr[2]));
      }
      this.policies.pets = this.policiesInfo.animals ? this.policiesInfo.animals : '';
      this.policies.children = this.policiesInfo.isAdultOnly ? this.policiesInfo.isAdultOnly : false;
      this.policiesInfo.children.forEach(item => {
        if (item.bad) {
          switch (item.bad) {
            case 'AVAILABLE_BEDS': {
              if (this.policies.adds[0].checked) {
                this.policies.adds[1].checked = true;
                if (item.price && item.price > 0) {
                  this.policies.adds[1].type = 'pay';
                  this.policies.adds[1].price = item.price;
                } else {
                  this.policies.adds[1].type = 'free';
                }
                this.policies.adds[1].agestart = item.ageFrom ? item.ageFrom.toString() : '';
                this.policies.adds[1].ageend = item.ageTo ? item.ageTo.toString() : '';
              } else {
                this.policies.adds[0].checked = true;
                if (item.price && item.price > 0) {
                  this.policies.adds[0].type = 'pay';
                  this.policies.adds[0].price = item.price;
                } else {
                  this.policies.adds[0].type = 'free';
                }
                this.policies.adds[0].agestart = item.ageFrom ? item.ageFrom.toString() : '';
                this.policies.adds[0].ageend = item.ageTo ? item.ageTo.toString() : '';
              }
              break;
            }
            case 'EXTRA_BEDS': {
              if (this.policies.adds[2].checked) {
                this.policies.adds[3].checked = true;
                if (item.price && item.price > 0) {
                  this.policies.adds[3].type = 'pay';
                  this.policies.adds[3].price = item.price;
                } else {
                  this.policies.adds[3].type = 'free';
                }
                this.policies.adds[3].agestart = item.ageFrom ? item.ageFrom.toString() : '';
                this.policies.adds[3].ageend = item.ageTo ? item.ageTo.toString() : '';
              } else {
                this.policies.adds[2].checked = true;
                if (item.price && item.price > 0) {
                  this.policies.adds[2].type = 'pay';
                  this.policies.adds[2].price = item.price;
                } else {
                  this.policies.adds[2].type = 'free';
                }
                this.policies.adds[2].agestart = item.ageFrom ? item.ageFrom.toString() : '';
                this.policies.adds[2].ageend = item.ageTo ? item.ageTo.toString() : '';
              }
              break;
            }
            case 'CHILDRENS_BEDS': {
              if (this.policies.adds[4].checked) {
                this.policies.adds[5].checked = true;
                if (item.price && item.price > 0) {
                  this.policies.adds[5].type = 'pay';
                  this.policies.adds[5].price = item.price;
                } else {
                  this.policies.adds[5].type = 'free';
                }
                this.policies.adds[5].agestart = item.ageFrom ? item.ageFrom.toString() : '';
                this.policies.adds[5].ageend = item.ageTo ? item.ageTo.toString() : '';
              } else {
                this.policies.adds[4].checked = true;
                if (item.price && item.price > 0) {
                  this.policies.adds[4].type = 'pay';
                  this.policies.adds[4].price = item.price;
                } else {
                  this.policies.adds[4].type = 'free';
                }
                this.policies.adds[4].agestart = item.ageFrom ? item.ageFrom.toString() : '';
                this.policies.adds[4].ageend = item.ageTo ? item.ageTo.toString() : '';
              }
              break;
            }
            default: {
              break;
            }
          }
        }
      });
      console.log(res);
      this.loaded = true;
    }, err => {
      if (err.status === 404) {
        this.policies = new Policies(6);
      }
      this.loaded = true;
    });
  }
  ngOnInit(): void {
  }
  onSubmit() {
    console.log(this.policies);
    this.policies.adds.forEach((item, index) => {
      if (!item.checked) {
        this.policies.adds[index] = new Add();
      }
    });
    this.policiesInfo = new PoliciesPost(this.policies);
    console.log(this.policiesInfo);
    this.generalservices.updatePolicies(this.policiesInfo).subscribe(res => {
      this.notificate = new Notify('post', 'success');
      }, err => {
      this.notificate = new Notify('post', 'error');
    });
  }
  initCheckbox(index: number): boolean {
    return this.policies.adds[index].checked;
  }
  onLimitsChange(index: number) {
    this.policies.adds[index].checked = !this.policies.adds[index].checked;
  }
  checkChildrenPrice(i: number) {
    if (this.policies.adds[i].type === 'free') {
      this.policies.adds[i].price = 0;
    }
  }
}
