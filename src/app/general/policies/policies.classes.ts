import * as moment from 'moment';


export class Policies {
  timestart: Date;
  timeend: Date;
  children: boolean;
  pets: string;
  adds: Add[];
  constructor(num: number) {
    this.children = false;
    this.pets = 'FORBIDDEN';
    this.timeend = new Date();
    this.timeend.setHours(12, 0, 0);
    this.timestart = new Date();
    this.timestart.setHours(14, 0, 0);
    this.adds = [];
    for (let i = 0; i < num; i++) {
      this.adds.push(new Add());
    }
  }
}
export class Add {
  agestart: string;
  ageend: string;
  type: string;
  checked: boolean;
  price: number;
  constructor() {
    this.ageend = '';
    this.agestart = '';
    this.type = '';
    this.checked = false;
    this.price = 0;
  }
}
export class PoliciesPost {
  arrival: string;
  departure: string;
  animals: string;
  isAdultOnly: boolean;
  children: Children[];
  constructor(policies: Policies) {
    this.arrival = moment(policies.timestart).format('HH:mm:ss');
    this.departure = moment(policies.timeend).format('HH:mm:ss');
    this.animals = policies.pets;
    this.isAdultOnly = policies.children;
    this.children = [];
    if (!this.isAdultOnly) {
      policies.adds.forEach((item, i) => {
        if (item.checked) {
          this.children.push(new Children(item, i));
        }
      });
    }
  }
}
export class Children {
  bad: string;
  ageFrom: number;
  ageTo: number;
  price: number;
  constructor(item: Add, i: number) {
    if (i < 2) { this.bad = 'AVAILABLE_BEDS'; }
    if (i === 2 || i === 3) { this.bad = 'EXTRA_BEDS'; }
    if (i > 3) { this.bad = 'CHILDRENS_BEDS'; }
    this.ageFrom = Number(item.agestart);
    this.ageTo = Number(item.ageend);
    this.price = item.price;
  }
}
