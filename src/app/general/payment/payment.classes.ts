export class Payments {
  isCashOnly: boolean;
  cards: any[];
  isCardRequired: boolean;
  constructor() {
    this.isCashOnly = false;
    this.isCardRequired = false;
    this.cards = [];
  }
}
