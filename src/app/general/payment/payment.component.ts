import { Component, OnInit } from '@angular/core';
import { Payments } from './payment.classes';
import {GeneralServices} from '../general.services';
import {forEach} from '@angular/router/src/utils/collection';
import {Notify} from '../../widgets/notification/notification.classes';

@Component({
  selector: 'payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.css']
})
export class PaymentComponent implements OnInit {
  notificate: any;
  loaded = false;
  cardsLoaded = false;
  payment: Payments;
  creditcards: any;
  wrongSubmittedForm: boolean;
  constructor(private generalservices: GeneralServices) {
  }
  ngOnInit(): void {
    this.generalservices.getCards().subscribe(res => {
      this.creditcards = res;
      this.cardsLoaded = true;
    });
    this.generalservices.getTax().subscribe(res => {
      this.payment = res;
      if (!res.isCashOnly) { this.payment.isCashOnly = false; }
      const arr = [];
      this.payment.cards.forEach(item => {
        arr.push(item.id);
      });
      this.payment.cards = arr;
      this.loaded = true;
    }, err => {
      if (err.status === 404) {
        this.payment = new Payments();
      }
      this.loaded = true;
    });
  }
  onSubmit() {
    if (this.payment.cards.length === 0 && !this.payment.isCashOnly) {
      this.wrongSubmittedForm = true;
    } else {
      this.wrongSubmittedForm = false;
      this.generalservices.postPayment(this.payment).subscribe(res => {
        this.notificate = new Notify('post', 'success');
      }, err => {
        this.notificate = new Notify('post', 'error');
      });
    }
  }
  opPaymentModChange() {
    this.payment.cards = [];
    this.payment.isCardRequired = false;
  }
  onPaymentChange(event, id: number): void {
    this.wrongSubmittedForm = false;
    if (event.target.checked) {
      this.payment.cards.push(id);
    } else {
      const index = this.payment.cards.indexOf(id);
      if (index > -1) {
        this.payment.cards.splice(index, 1);
      }
    }
  }
  initCheckbox(id: number): boolean {
    return this.payment.cards.indexOf(id) > -1;
  }
}
