import { Component, OnInit, TemplateRef } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import {Contact, ContactForm} from './contacts.classes';
import {GeneralServices} from '../general.services';
import {Notify} from '../../widgets/notification/notification.classes';

@Component({
  selector: 'contacts',
  templateUrl: './contacts.component.html',
  styleUrls: ['./contacts.component.css']
})
export class ContactsComponent implements OnInit {
  notificate: any;
  loaded = false;
  modalRef: BsModalRef;
  openFormMod: string;
  contactForm: ContactForm;
  contacts: Contact[];
  main_contact: Contact;
  booking_contact: Contact;
  tariffs_contact: Contact;
  contract_contact: Contact;
  content_contact: Contact;
  availability_contact: Contact;
  openFormModTitle: string;
  phoneMask = ['+', /\d/, /\d/, ' ', '(', /\d/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
  emailPattern = '^[a-z0-9._%+-]+@[a-z0-9.-]{1,}\.[a-z]{2,4}$';
  constructor(private modalService: BsModalService, private generalservices: GeneralServices) {
    this.loadAll();
  }
  loadAll() {
    this.generalservices.getContacts().subscribe(res => {
      this.contacts = res;
      this.contacts.forEach(item => {
        switch (item.type) {
          case 'MAIN': {
            this.main_contact = item;
            break;
          }
          case 'BOOKING': {
            this.booking_contact = item;
            break;
          }
          case 'AVAILABILITY': {
            this.availability_contact = item;
            break;
          }
          case 'TARIFFS': {
            this.tariffs_contact = item;
            break;
          }
          case 'CONTENT': {
            this.content_contact = item;
            break;
          }
          case 'CONTRACT': {
            this.contract_contact = item;
            break;
          }
          default: {
            break;
          }
        }
      });
      console.log(res);
      this.loaded = true;
    }, err => {
      if (err.status === 404) {
        this.contacts = [];
      }
      this.loaded = true;
    });
  }
  ngOnInit() {
    this.contactForm = new ContactForm();
  }
  convertData(contact: Contact) {
    this.contactForm.position = contact.position ? contact.position : '';
    this.contactForm.email = contact.email ? contact.email : '';
    this.contactForm.name = contact.name ? contact.name : '';
    this.contactForm.phone = contact.phone ? contact.phone : '';
  }
  openForm(template: TemplateRef<any>, mod: string) {
    switch (mod) {
      case 'general': {
        this.openFormMod = 'MAIN';
        this.openFormModTitle = 'Основной контакт';
        this.contactForm = new ContactForm();
        this.contactForm.type = this.openFormMod;
        if (this.main_contact) {
          this.convertData(this.main_contact);
        }
        this.modalRef = this.modalService.show(template);
        break;
      }
      case 'places': {
        this.openFormMod = 'AVAILABILITY';
        this.openFormModTitle = 'Наличие мест';
        this.contactForm = new ContactForm();
        this.contactForm.type = this.openFormMod;
        if (this.availability_contact) {
          this.convertData(this.availability_contact);
        }
        this.modalRef = this.modalService.show(template);
        break;
      }
      case 'tariffs': {
        this.openFormMod = 'TARIFFS';
        this.openFormModTitle = 'Установление тарифов';
        this.contactForm = new ContactForm();
        this.contactForm.type = this.openFormMod;
        if (this.tariffs_contact) {
          this.convertData(this.tariffs_contact);
        }
        this.modalRef = this.modalService.show(template);
        break;
      }
      case 'agreement': {
        this.openFormMod = 'CONTRACT';
        this.openFormModTitle = 'Контакт';
        this.contactForm = new ContactForm();
        this.contactForm.type = this.openFormMod;
        if (this.contract_contact) {
          this.convertData(this.contract_contact);
        }
        this.modalRef = this.modalService.show(template);
        break;
      }
      case 'bookings': {
        this.openFormMod = 'BOOKING';
        this.openFormModTitle = 'Бронирования';
        this.contactForm = new ContactForm();
        this.contactForm.type = this.openFormMod;
        if (this.booking_contact) {
          this.convertData(this.booking_contact);
        }
        this.modalRef = this.modalService.show(template);
        break;
      }
      case 'view': {
        this.openFormMod = 'CONTENT';
        this.openFormModTitle = 'Фотографии и описание';
        this.contactForm = new ContactForm();
        this.contactForm.type = this.openFormMod;
        if (this.content_contact) {
          this.convertData(this.content_contact);
        }
        this.modalRef = this.modalService.show(template);
        break;
      }
      default: {
        break;
      }
    }
  }
  onSubmitContact() {
    // console.log(this.contactForm);
    let dataId: number;
    switch (this.contactForm.type) {
      case 'MAIN': {
        if (this.main_contact) {
          dataId = this.main_contact.id;
        }
        break;
      }
      case 'BOOKING': {
        if (this.booking_contact) {
          dataId = this.booking_contact.id;
        }
        break;
      }
      case 'AVAILABILITY': {
        if (this.availability_contact) {
          dataId = this.availability_contact.id;
        }
        break;
      }
      case 'TARIFFS': {
        if (this.tariffs_contact) {
          dataId = this.tariffs_contact.id;
        }
        break;
      }
      case 'CONTENT': {
        if (this.content_contact) {
          dataId = this.content_contact.id;
        }
        break;
      }
      case 'CONTRACT': {
        if (this.contract_contact) {
          dataId = this.contract_contact.id;
        }
        break;
      }
      default: {
        break;
      }
    }
    if (dataId) {
      this.generalservices.updateContact(this.contactForm, dataId).subscribe(res => {
        this.notificate = new Notify('post', 'success');
        this.loadAll();
      }, err => {
        this.notificate = new Notify('post', 'error');
      });
    } else {
      this.generalservices.addContact(this.contactForm).subscribe(res => {
        this.notificate = new Notify('post', 'success');
        this.loadAll();
      }, err => {
        this.notificate = new Notify('post', 'error');
      });
    }
    this.modalRef.hide();
  }
}
