export class ContactForm {
  name: string;
  position: string;
  email: string;
  phone: string;
  type: string;
  constructor() {
    this.name = '';
    this.email = '';
    this.phone = '';
    this.position = '';
    this.type = '';
  }
}
export class Contacts {
  contact: Contact[];
}
export class Contact {
  id: number;
  name: string;
  email: string;
  phone: string;
  type: string;
  position: string;
}
