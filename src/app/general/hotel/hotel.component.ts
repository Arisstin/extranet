import { Component, OnInit } from '@angular/core';

import { Hotel } from './hotel';
import { HotelService } from './hotel.service';

@Component({
  selector: 'hotel-data',
  templateUrl: './hotel.component.html',
  styleUrls: ['./hotel.component.css']
})
export class HotelComponent implements OnInit {

 	hotel: Hotel;
 	loaded = false;
 	stars: number[];
  constructor(
    private hotelService: HotelService,
  ) {}

  ngOnInit(): void {
   this.hotelService.getHotel().subscribe(hotel => {
     this.hotel = hotel;
     this.stars = Array(this.hotel.stars).fill(1);
     this.loaded = true; }, error => {this.loaded = true;}); //Bind to view
  }

}
