export class Hotel {
  id: number;
  title: string;
  stars: number;
}
