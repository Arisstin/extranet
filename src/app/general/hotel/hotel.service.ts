import { Injectable }    from '@angular/core';
import { Headers, Response} from '@angular/http';
import { HttpClient } from '@angular/common/http';

import {Observable} from 'rxjs/Rx';
import { Globals } from '../../globals';
import { Hotel } from './hotel';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class HotelService {

  private headers = new Headers({'Content-Type': 'application/json'});
  private heroesUrl: string;

  constructor(private http: HttpClient, private globals: Globals) {
    this.heroesUrl = this.globals.BACK_END + 'ru/extranet/hotel';
  }

  getHotel(): Observable<any> {
    return this.http.get<Hotel>(this.heroesUrl);
  }

}
