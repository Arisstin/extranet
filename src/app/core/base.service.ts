import { Injectable } from '@angular/core';
import { Headers, Response} from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { Globals } from '../globals';
import { LocalizeRouterService } from 'localize-router';

import {Observable} from 'rxjs/Rx';

@Injectable()
export class BaseService{

  private baseUrl: string;

  private api = 'extranet';

  constructor(
  	private http: HttpClient,
  	private localizeService: LocalizeRouterService,
    private globals: Globals
  	) {
    this.baseUrl = this.globals.BACK_END;
  }

  get(url: string, data?: any): Observable<any> {
    return this.http.get<any>(this.getBaseUrl() + url, data);
  }

  post(url: string, data: any): Observable<any> {
  	return this.http.post<any>(this.getBaseUrl() + url, data);
  }

  put(url: string, data: any): Observable<any> {
  	return this.http.post<any>(this.getBaseUrl() + url, data);
  }

  getBaseUrl(): string {
  	return this.baseUrl + this.localizeService.parser.currentLang + '/' + this.api + '/';
  }

   getWithParams(url: string, params: any): Observable<any> {
    return this.http.get<any>(this.getBaseUrl() + url, params);
  }

}
