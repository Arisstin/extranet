import * as moment from 'moment';

export class BaseForm {
  persent: number;
  dates: Date[];
  days: number[];
  tariffs: number[];
  hoursBeforeCheckln: number;
  constructor() {
    this.persent = 0;
    this.days = [];
    this.tariffs = [];
    this.dates = [,];
    this.hoursBeforeCheckln = 6;
  }
}
export class BasePost {
  period: Period;
  percent: number;
  type: string;
  hoursBeforeCheckln: number;
  weekdays: number[];
  constructor(dates: Date[], days: number[], percent: number, hoursBeforeCheckln: number) {
    this.type = 'LAST_MINUTE';
    this.percent = percent;
    this.period = new Period(dates, days);
    this.hoursBeforeCheckln = Number(hoursBeforeCheckln);
  }
}
export class Period {
  from: string;
  to: string;
  weekdays: number[];
  constructor(dates: Date[], days: number[]) {
    this.from = moment(dates[0]).format('YYYY-MM-DD');
    this.to = moment(dates[1]).format('YYYY-MM-DD');
    this.weekdays = days;
  }
}
export class Tariffs {
  id: number;
  title: string;
}
