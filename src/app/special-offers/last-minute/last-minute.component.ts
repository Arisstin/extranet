import { Component, OnInit } from '@angular/core';
import {BsDatepickerConfig} from 'ngx-bootstrap';
import {SpecialOffersServices} from '../special-offers.services';
import { BaseForm, Tariffs, BasePost } from './last-minute.classes';
import {Notify} from '../../widgets/notification/notification.classes';

@Component({
  selector: 'last-minute',
  templateUrl: './last-minute.component.html',
  styleUrls: ['./last-minute.component.css']
})
export class LastMinuteComponent implements OnInit {
  notificate: any;
  loaded = false;
  colorTheme = 'theme-red';
  bsConfig: Partial<BsDatepickerConfig>;
  weekdays = [
    {id: 1, title: 'пн'},
    {id: 2, title: 'вт'},
    {id: 3, title: 'ср'},
    {id: 4, title: 'чт'},
    {id: 5, title: 'пт'},
    {id: 6, title: 'сб'},
    {id: 7, title: 'вс'}];
  minPercent = 0;
  maxPercent = 100;
  tariffs: Tariffs[];
  baseForm: BaseForm;
  existWeekdays = false;
  basePost: BasePost;
  MinDates: Date[] = [new Date(), new Date()];
  constructor(private specialoffersservices: SpecialOffersServices) {
    this.baseForm = new BaseForm();
    this.baseForm.dates = [new Date(), new Date()];
    this.baseForm.dates[1].setDate(this.baseForm.dates[0].getDate() + 1);
    this.MinDates[1] = this.baseForm.dates[1];
    this.specialoffersservices.getTariffs().subscribe(res => {
      this.tariffs = res;
      this.loaded = true;
    }, err => this.loaded = true);
  }
  dateChanged(value) {
    if (value !== null) {
      const day = new Date();
      day.setFullYear(value.getFullYear(), value.getMonth(), value.getDate() + 1);
      this.MinDates[1] = day;
      if (value > this.baseForm.dates[1] || (value.getDate() === this.baseForm.dates[1].getDate() &&
          value.getMonth() === this.baseForm.dates[1].getMonth() && value.getFullYear() === this.baseForm.dates[1].getFullYear())) {
        this.baseForm.dates[1] = day;
      }
    }
  }
  ngOnInit(): void {
    this.bsConfig = Object.assign({}, {
      containerClass: this.colorTheme,
      showWeekNumbers: false,
      dateInputFormat: 'dd, D MMMM YYYY',
      displayMonths: 2
    });
  }
  initCheckboxDay(id: number) {
    return this.baseForm.days.indexOf(id) > -1;
  }
  initCheckTariff(id: number) {
    return this.baseForm.tariffs.indexOf(id) > -1;
  }
  selectAllDays() {
    this.baseForm.days = [1, 2, 3, 4, 5, 6, 7];
  }
  unselectAllDays() {
    this.baseForm.days = [];
  }
  onDayChange(event, id: number) {
    this.existWeekdays = false;
    if (event.target.checked) {
      this.baseForm.days.push(id);
    } else {
      const index = this.baseForm.days.indexOf(id);
      if (index > -1) {
        this.baseForm.days.splice(index, 1);
      }
    }
  }
  onSubmit() {
    if (this.baseForm.days.length > 0) {
      this.basePost = new BasePost(this.baseForm.dates, this.baseForm.days, this.baseForm.persent, this.baseForm.hoursBeforeCheckln);
      if (this.baseForm.tariffs.length > 0) {
        this.baseForm.tariffs.forEach(item => {
          this.specialoffersservices.postDiscount(item, this.basePost).subscribe(
            tariff => this.notificate = new Notify('post', 'success'),
            err => this.notificate = new Notify('post', 'error')
          );
        });
      }
    } else {
      this.existWeekdays = true;
    }
  }
  onTariffChange(event, id) {
    if (event.target.checked) {
      this.baseForm.tariffs.push(id);
    } else {
      const index = this.baseForm.tariffs.indexOf(id);
      if (index > -1) {
        this.baseForm.tariffs.splice(index, 1);
      }
    }
  }
  selectAllTriffs() {
    this.baseForm.tariffs = [];
    this.tariffs.forEach(item => {
      this.baseForm.tariffs.push(item.id);
    });
  }
  unselectAllTariffs() {
    this.baseForm.tariffs = [];
  }
}
