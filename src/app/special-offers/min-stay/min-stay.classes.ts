import * as moment from 'moment';

export class BaseForm {
  persent: number;
  dates: Date[];
  days: number[];
  tariffs: number[];
  minNights: string;
  constructor() {
    this.persent = 0;
    this.days = [];
    this.tariffs = [];
    this.dates = [,];
    this.minNights = '2';
  }
}
export class BasePost {
  period: Period;
  percent: number;
  type: string;
  minNights: number;

  constructor(dates: Date[], days: number[], percent: number, minNights: string) {
    this.percent = percent;
    this.type = 'MIN_STAY';
    this.minNights = Number(minNights);
    this.period = new Period(dates, days);
  }
}
export class Period {
  from: string;
  to: string;
  weekdays: number[];
  constructor(dates: Date[], days: number[]) {
    this.from = moment(dates[0]).format('YYYY-MM-DD');
    this.to = moment(dates[1]).format('YYYY-MM-DD');
    this.weekdays = days;
  }
}
export class Tariffs {
  id: number;
  title: string;
}
