import { Injectable } from '@angular/core';
import {Observable} from 'rxjs/Rx';
import { Tariffs } from './base/base.classes';

import { BaseService } from '../core/base.service';

@Injectable()
export class SpecialOffersServices extends BaseService {

  public getTariffs(): Observable<Tariffs[]> {
    return this.get('tariffs');
  }
  public postDiscount(tariffid: number, data: any): Observable<any> {
    return this.post('tariffs/' + tariffid + '/add-discount', data);
  }
}
