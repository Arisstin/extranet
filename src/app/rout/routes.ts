import { RouterModule, Routes } from '@angular/router';

import { HotelComponent } from '../general/hotel/hotel.component';
import { WebSiteComponent} from '../submenues/web-site.component';
import { GeneralInfoComponent } from '../submenues/general-info.component';
import { RoomsComponent } from '../website/rooms/rooms.component';
import { AddRoomComponent } from '../website/rooms/add-room.component';
import { FacilityComponent } from '../website/facility/facility.component';
import { TariffMenueComponent } from '../submenues/tariff-menue.component';
import { AddTariffComponent } from '../tariffs/add-tariff.component';
import { ReviewComponent } from '../tariffs/review/review.component';
import { PhotoComponent } from '../website/photo/photo.component';
import { SpecialOfferComponent } from '../submenues/special-offer.component';
import { BaseDiscountComponent } from '../special-offers/base/base-discount.component';
import { LastMinuteComponent } from '../special-offers/last-minute/last-minute.component';
import { MinStayComponent } from '../special-offers/min-stay/min-stay.component';
import { ContactsComponent } from '../general/contacts/contacts.component';
import { LandmarksComponent } from '../general/landmarks/landmarks.component';
import { PaymentComponent } from '../general/payment/payment.component';
import { TaxesComponent } from '../general/taxes/taxes.component';
import { PoliciesComponent } from '../general/policies/policies.component';
import { BookingComponent } from '../booking/booking.component';
import { AuthPageComponent } from '../auth/auth-page/auth-page.component';
import { DescriptionComponent } from '../website/description/description.component';
import {FinanceComponent} from '../finance/finance.component';
import {BookingConfirmComponent} from '../booking/booking-confirm/booking-confirm.component';
import {BookingTableComponent} from '../booking/booking-table/booking-table.component';

export const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'login', component: AuthPageComponent },
  { path: 'login/:id', component: AuthPageComponent },
  { path: 'general',  component: GeneralInfoComponent,
    children: [
      { path: '',  redirectTo: 'hotel', pathMatch: 'full' },
      { path: 'contacts', component: ContactsComponent },
      { path: 'hotel', component: HotelComponent },
      { path: 'landmarks', component: LandmarksComponent },
      { path: 'payment', component: PaymentComponent },
      { path: 'taxes', component: TaxesComponent },
      { path: 'policies', component: PoliciesComponent }
      ],
    },
  { path: 'website', component: WebSiteComponent,
  	children: [
  	  { path: '', redirectTo: 'rooms', pathMatch: 'full' },
  		{ path: 'rooms', component: RoomsComponent },
  		{ path: 'room/add', component: AddRoomComponent },
  		{ path: 'rooms/:id', component: AddRoomComponent },
      { path: 'photo', component: PhotoComponent },
      { path: 'facility', component: FacilityComponent },
      { path: 'description', component: DescriptionComponent}
  	],
	},
	{ path: 'tariffs',  component: TariffMenueComponent,
		children: [
      { path: '', redirectTo: 'review', pathMatch: 'full' },
  		{ path: 'create', component:	AddTariffComponent },
      { path: 'review', component: ReviewComponent }
  	]
	},
  { path: 'spacial-offer',  component: SpecialOfferComponent,
    children: [
      { path: '', redirectTo: 'base', pathMatch: 'full' },
      { path: 'base', component: BaseDiscountComponent },
      { path: 'last-minute', component: LastMinuteComponent },
      { path: 'min-stay', component: MinStayComponent }
    ]
  },
  { path: 'booking', component: BookingComponent },
  { path: 'booking/:id', component: BookingTableComponent },
  { path: 'booking-confirm', component: BookingConfirmComponent },
  { path: 'finance', component: FinanceComponent },
  { path: '**', redirectTo: 'login', pathMatch: 'full' }

];

