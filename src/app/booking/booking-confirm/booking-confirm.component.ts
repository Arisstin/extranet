import { Component, OnInit } from '@angular/core';
import { BookingServices } from '../booking.services';
import { Router } from '@angular/router';

@Component({
  selector: 'app-booking-confirm',
  templateUrl: './booking-confirm.component.html',
  styleUrls: ['./booking-confirm.component.css']
})
export class BookingConfirmComponent implements OnInit {
  displayBookingInfo = false;
  currentBookingId: number;

  constructor(private bookingservices: BookingServices, private router: Router) {

  }

  ngOnInit() {
  }
  closeFullInfo() {
    this.displayBookingInfo = false;
  }
  openFullInfo(id: number) {
    this.currentBookingId = id;
    this.displayBookingInfo = true;
  }
}
