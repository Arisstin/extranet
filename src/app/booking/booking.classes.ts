import * as moment from 'moment';
export class SearchForm {
  s_by: string;
  from: Date;
  to: Date;
  keywords: string;
  constructor() {
    this.s_by = 'checkOut';
    this.keywords = '';
  }
  convert() {
    return {
      s_by: this.s_by,
      to: moment(this.to).format('YYYY-MM-DD'),
      from: moment(this.from).format('YYYY-MM-DD'),
      keywords: this.keywords
    };
  }
}
export class BookingPost {
  s_by: string;
  from: string;
  to: string;
  keywords: string;
  offset: number;
  limit: number;
  sort_by?: string;
  order?: string;
  constructor(searchForm: SearchForm, offset: number, limit: number) {
    this.s_by = searchForm.s_by;
    this.from = moment(searchForm.from).format('YYYY-MM-DD');
    this.to = moment(searchForm.to).format('YYYY-MM-DD');
    this.keywords = searchForm.keywords;
    this.offset = offset;
    this.limit = limit;
  }
}
export class Bookings {
  total: number;
  booking: Booking[];
}
export class Booking {
  id: number;
  isVeawed: boolean;
  bookingDate: string;
  checkIn: string;
  checkOut: string;
  status: string;
  price: number;
  commission: number;
  priceOfNoShow: number;
  priceForCommition: number;
  priceOfCanceld: number;
}
export class Status {
  status: string;
}
