import { Injectable } from '@angular/core';
import {Observable} from 'rxjs/Rx';
import { BaseService } from '../core/base.service';
import {BookingPost, Bookings} from './booking.classes';

@Injectable()
export class BookingServices extends BaseService {
  getBookings(bookInfo: BookingPost): Observable<Bookings> {
    const params = this.convertObjectToParms(bookInfo);
    return  this.get('bookings?' + params);
  }
  getBookingsUnconfirmed(): Observable<Bookings> {
    return  this.get('bookings?status=BOOKED');
  }
  confirmBookingsUnconfirmed(): Observable<any> {
    return this.post('bookings/confirm', {});
  }
  bookConfirm(id: number): Observable<any> {
    return this.post('booking/' + id + '/confirm', {});
  }
  bookViewed(id: number): Observable<any> {
   return this.post('bookings/' + id + '/view', null);
  }
  bookCancel(id: number): Observable<any> {
    return this.post('bookings/' + id + '/cancel', null);
  }
  bookNotVisited(id: number): Observable<any> {
    return this.post('bookings/' + id + '/noshow', null);
  }
  bookInvalidCard(id: number): Observable<any> {
    return this.post('bookings/' + id + '/invalid-card', null);
  }
  bookEditBookingRoom(id: number, roomid: number, data: any): Observable<any> {
    return this.post('bookings/' + id + '/rooms/' + roomid + '/change-dates', data);
  }
  bookEditCancelBookingRoom(id: number, roomid: number): Observable<any> {
    return this.post('bookings/' + id + '/rooms/' + roomid + '/cancel', null);
  }
  getRoomsOfBooking(id: number): Observable<any> {
    return this.get('bookings/' + id);
  }
  convertStringToDate(value: string) {
    const dates = value.split('-');
    return new Date(Number(dates[0]), Number(dates[1]) - 1, Number(dates[2]), 0, 0);
  }
  convertObjectToParms(obj: any) {
    const params = new URLSearchParams();
    for(const key in obj) {
      params.set(key, obj[key]);
    }
    return params;
  }
  downloadExel(data: any): Observable<any> {
    return this.get('bookings/exel', data);
  }
}
