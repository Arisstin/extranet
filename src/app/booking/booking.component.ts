import { Component, OnInit, ViewChild, AfterViewInit, TemplateRef } from '@angular/core';
import {BookingPost, Bookings, SearchForm} from './booking.classes';
import { BsDatepickerConfig, BsModalRef, BsModalService } from 'ngx-bootstrap';
import { BookingServices } from './booking.services';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Subject';
import {Notify} from '../widgets/notification/notification.classes';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import * as moment from 'moment';
import {Location} from '@angular/common';

@Component({
  selector: 'booking',
  templateUrl: './booking.component.html',
  styleUrls: ['./booking.component.css']
})
export class BookingComponent implements OnInit, AfterViewInit {
  notificate: any;
  dtOptions: DataTables.Settings = {};
  searchForm: SearchForm;
  bookingPostInfo: BookingPost;
  bookings: Bookings;
  modalRef: BsModalRef;
  modalRef2: BsModalRef;
  minDate = new Date();
  minDateEdit = new Date();
  minDateEditFrom = new Date();
  confirmText: string;
  bookRoomsInfo: any;
  confirmMod: string;
  confirmIdBook: number;
  colorTheme = 'theme-dark-blue';
  bsConfig: Partial<BsDatepickerConfig>;
  bsConfig2: Partial<BsDatepickerConfig>;
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  dtTrigger: Subject<any> = new Subject();
  tableLenght: number;
  searchCount = 0;
  needConfirmBookings: any;
  displayBookingInfo = false;   ////////////
  currentBookingId: number;
  searchParams = false;

  constructor(private bookingservices: BookingServices,
              private modalService: BsModalService,
              private router: Router,
              private activeroute: ActivatedRoute,
              private location: Location) {
    this.minDateEdit.setDate(this.minDateEditFrom.getDate() + 1);
    this.searchForm = new SearchForm();
    console.log(this.searchForm);
    this.activeroute.queryParams.subscribe( res => {
      if (res.from) {
        this.searchParams = true;
        this.searchForm.from = this.bookingservices.convertStringToDate(res.from);
      }
      if (res.to) { this.searchForm.to = this.bookingservices.convertStringToDate(res.to); }
      if (res.s_by) { this.searchForm.s_by = res.s_by; }
      if (res.keywords) { this.searchForm.keywords = res.keywords; }
    });
    localStorage.removeItem('confirmWindows');
    this.bookingPostInfo = new BookingPost(this.searchForm, 0, 1);
    this.bookingservices.getBookingsUnconfirmed().subscribe(res => {
      this.needConfirmBookings = res;
    });
  }
  ngOnInit(): void {
    const that = this;
    this.bsConfig = Object.assign({}, { containerClass: this.colorTheme, showWeekNumbers: false, dateInputFormat: 'dd, D MMMM YYYY' });
    this.bsConfig2 = Object.assign({}, { containerClass: this.colorTheme, showWeekNumbers: false, dateInputFormat: 'YYYY-MM-DD' });
    this.dtOptions = {
      pagingType: 'first_last_numbers',
      lengthMenu:  [ [50, 100, 150, 200, -1], [50, 100, 150, 200, 'All'] ],
      searching: false,
      processing: true,
      // ordering: false,
      serverSide: true,
      info: false,
      // language: {
      //   search: 'Поиск',
      //   processing: 'Загрузка',
      //   lengthMenu: 'Отобразить _MENU_ записей',
      //   info: 'Показано с _START_ по _END_ из _TOTAL_ бронирований',
      //   infoEmpty: 'Бронирований не найдено',
      //   paginate: {
      //     last: '<i class="fa fa-forward"></i>',
      //     first: '<i class="fa fa-backward"></i>'
      //   }
      // },
      columns: [
        { data: 'id', orderable: false },
        { data: 'booking' },
        { data: 'check_in' },
        { data: 'check_out' },
        { data: 'status', orderable: false },
        { data: 'price', orderable: false },
        { data: 'commission', orderable: false }],
      ajax: (dataTablesParameters: any, callback) => {
        console.log(dataTablesParameters, 'ok');
        this.bookingPostInfo.limit = dataTablesParameters.length;
        this.bookingPostInfo.offset = dataTablesParameters.start;
        if (dataTablesParameters.order[0].column !== 0) {
          this.bookingPostInfo.sort_by = dataTablesParameters.columns[dataTablesParameters.order[0].column].data;
          this.bookingPostInfo.order = dataTablesParameters.order[0].dir;
        }
        if (this.bookingPostInfo.limit === -1) {
          delete this.bookingPostInfo.limit;
          this.bookingPostInfo.offset = 0;
        }
        this.tableLenght = dataTablesParameters.length;
          if (this.searchCount === 0 && !this.searchParams) {
            delete this.bookingPostInfo.to;
            this.bookingPostInfo.s_by = 'checkOut';
          }
        this.bookingservices.getBookings(this.bookingPostInfo).subscribe(res => {
          this.searchCount++;
          that.bookings = res;
          callback({
            recordsTotal: res.total,
            recordsFiltered: res.total,
            data: []
          });
        });
      }
    };
  }
  bookConfirm(id: number, index: number) {
    this.bookingservices.bookConfirm(id).subscribe(res => {
      if (this.bookings.booking[index].status === 'BOOKED') {
        this.bookings.booking[index].status = 'CONFIRMED';
      }
    });
  }
  bookConfirming(id: number, mod: string) {
    this.modalRef.hide();
    switch (mod) {
      case 'cf': {
        this.bookViewed(id);
        break;
      }
      case 'cl': {
        this.bookCancel(id);
        break;
      }
      case 'nv': {
        this.bookNotVisit(id);
        break;
      }
      case 'ic': {
        this.bookInvalidCard(id);
        break;
      }
      default: {
        break;
      }
    }
  }
  bookViewed(id: number) {
    this.bookingservices.bookViewed(id).subscribe(res => {
      const indexview = this.bookings.booking.findIndex(i => i.id === id);
      this.bookings.booking[indexview].isVeawed = true;
      console.log(res);
    });
    console.log('viewed');
  }
  bookCancel(id: number) {
    this.bookingservices.bookCancel(id).subscribe(res => {
      const indexcancel = this.bookings.booking.findIndex(i => i.id === id);
      this.bookings.booking[indexcancel].status = 'CANCELED_BY_HOTEL';
    });
  }
  bookInvalidCard(id: number) {
    this.bookingservices.bookInvalidCard(id).subscribe(res => { console.log(res); });
    console.log('invalid');
  }
  bookNotVisit(id: number) {
    this.bookingservices.bookNotVisited(id).subscribe(res => {
      const indexnoshow = this.bookings.booking.findIndex(i => i.id === id);
      this.bookings.booking[indexnoshow].status = 'NO_SHOW';
    });
  }
  closeFullInfo() {
    this.displayBookingInfo = false;
  }
  closeEdit() {
    this.modalRef2.hide();
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.destroy();
      // this.dtOptions.pageLength = 15;
      this.dtTrigger.next();
    });
  }
  closeRoom(id: number, roomid: number) {
    this.bookingservices.bookEditCancelBookingRoom(id, roomid).subscribe(res => {
      const indexcancel = this.bookRoomsInfo.rooms.findIndex(i => i.id === id);
      this.bookRoomsInfo.rooms[indexcancel].status = 'CANCELED_BY_HOTEL';
    });
  }
  confirmUnconfirmed() {
    this.bookingservices.confirmBookingsUnconfirmed().subscribe(res => {
      this.needConfirmBookings = undefined;
      this.searchCount = 0;
      this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
        dtInstance.destroy();
        this.dtTrigger.next();
      });
    });
  }
  countCancel() {
    let summ = 0;
    this.bookings.booking.forEach(item => {
      summ += item.priceOfCanceld;
    });
    return summ;
  }
  countComission() {
    let summ = 0;
    this.bookings.booking.forEach(item => {
        summ += item.commission;
    });
    return summ;
  }
  countPrice() {
    let summ = 0;
    this.bookings.booking.forEach(item => {
      summ += item.priceForCommition;
    });
    return summ;
  }
  countUnvisit() {
    let summ = 0;
    this.bookings.booking.forEach(item => {
      summ += item.priceOfNoShow;
    });
    return summ;
  }
  downloadExel() {
    return this.bookingservices.downloadExel({ responseType: 'blob', params: this.bookingPostInfo }).subscribe(res => {
      console.log('start download:', res);
      const binaryData = [];
      binaryData.push(res);
      const url = window.URL.createObjectURL(new Blob(binaryData, {type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'}));
      const a = document.createElement('a');
      document.body.appendChild(a);
      a.setAttribute('style', 'display: none');
      a.href = url;
      a.download = 'file_.xls';
      a.click();
      window.URL.revokeObjectURL(url);
      a.remove(); // remove the element
    }, error => {
      console.log('download error:', JSON.stringify(error));
    }, () => {
      console.log('Completed file download.');
    });
  }
  disabledButton(mod: string, dates: any[], status: string) {
    const currdat = new Date();
    if ((status === 'ACCOMMODATION_IS_CONFIRMED' && mod !== 'edit') || status === 'CANCELED_BY_HOTEL' || status === 'NO_SHOW') {
      return true;
    }
    let noshow = true;
    switch (mod) {
      case 'edit': {
        dates.forEach(item => {
          const dat = this.bookingservices.convertStringToDate(item.checkOut);
          if ((currdat.valueOf() - dat.valueOf()) / 3600000 < 12) {
            noshow = false;
          }
        });
        break;
      }
      case 'nv': {
        dates.forEach(item => {
          const dat = this.bookingservices.convertStringToDate(item.checkIn);
          if (((currdat.valueOf() - dat.valueOf()) / 3600000 > 36 || (currdat.valueOf() - dat.valueOf()) / 3600000 < 0) && item.status !== 'CANCELED_BY_HOTEL' && item.status !== 'NO_SHOW') { noshow = false; }
        });
        noshow = !noshow;
        break;
      }
      case 'cl': {
        dates.forEach(item => {
          const dat = this.bookingservices.convertStringToDate(item.checkIn);
          if ((currdat.valueOf() - dat.valueOf()) / 3600000 > 36 && item.status !== 'CANCELED_BY_HOTEL' && item.status !== 'NO_SHOW') { noshow = false; }
        });
        noshow = !noshow;
        break;
      }
      default: {
        noshow = true;
        break;
      }
    }
    return noshow;
  }
  dateChanged(value) {
    if (value !== null) {
      const day = new Date();
      day.setFullYear(value.getFullYear(), value.getMonth(), value.getDate() + 1);
      this.minDate = day;
      if (value > this.searchForm.to ||
        (value.getDate() === this.searchForm.to.getDate()
          && value.getMonth() === this.searchForm.to.getMonth()
          && value.getFullYear() === this.searchForm.to.getFullYear())) {
        this.searchForm.to = day;
      }
    }
  }
  dateChanged1(value, roomindex: number) {
    if (value !== null) {
      this.bookRoomsInfo.changed[roomindex] = true;
      const day = new Date();
      day.setFullYear(value.getFullYear(), value.getMonth(), value.getDate() + 1);
      this.minDateEdit = day;
      if (value > this.bookRoomsInfo.rooms[roomindex].checkOut ||
        (value.getDate() >= this.bookRoomsInfo.rooms[roomindex].checkOut.getDate()
          && value.getMonth() >= this.bookRoomsInfo.rooms[roomindex].checkOut.getMonth()
          && value.getFullYear() >= this.bookRoomsInfo.rooms[roomindex].checkOut.getFullYear())) {
        this.bookRoomsInfo.rooms[roomindex].checkOut = day;
      }
    }
  }
  dateChanged2(roomindex: number) {
    this.bookRoomsInfo.changed[roomindex] = true;
  }
  ngAfterViewInit(): void {
    this.dtTrigger.next();
  }
  openConfirm(template: TemplateRef<any>, id: number, mod: string) {
    this.confirmMod = mod;
    this.confirmIdBook = id;
    switch (mod) {
      case 'cf': {
          this.confirmText = 'Утвердить данное бронирование как просмотренное? Это будет означать что вы согласны с ' +
            'пожеланиями клиента и ' +
            'готовы предоставить ему указанные номер(а)';
          this.modalRef = this.modalService.show(template, {class: 'modal-viewed modal-window'});
        break;
      }
      case 'cl': {
          this.confirmText = 'Отменить данное бронирование?';
          this.modalRef = this.modalService.show(template, {class: 'modal-cancel modal-window'});
        break;
      }
      case 'nv': {
          this.confirmText = 'Клиент не посетил забронированный номер?';
          this.modalRef = this.modalService.show(template, {class: 'modal-invalid-card modal-window'});
        break;
      }
      case 'ic': {
          this.confirmText = 'Клиент предоставил неправильные данные свой кредитной карты?';
          this.modalRef = this.modalService.show(template, {class: 'modal-not-visit modal-window'});
        break;
      }
      case 'edit': {
        this.bookingservices.getRoomsOfBooking(id).subscribe(res => {
          console.log(res);
          this.bookRoomsInfo = res;
          this.bookRoomsInfo['changed'] = Array(this.bookRoomsInfo.rooms.length).fill(false);
          this.currentBookingId = this.bookRoomsInfo.id;
          this.bookRoomsInfo.rooms.forEach((item, index) => {
            this.bookRoomsInfo.rooms[index].checkIn = this.bookingservices.convertStringToDate(this.bookRoomsInfo.rooms[index].checkIn);
            this.bookRoomsInfo.rooms[index].checkOut = this.bookingservices.convertStringToDate(this.bookRoomsInfo.rooms[index].checkOut);
          });
          this.modalRef2 = this.modalService.show(template, {class: 'modal-window-edit', ignoreBackdropClick: true});
        });
        break;
      }
      default: {
        break;
      }
    }
  }
  roomPrice(arr: any) {
    let summ = 0;
    arr.forEach(item => {
      summ += item.price;
    });
    return summ;
  }
  onSubmit() {
    this.bookings = {total: 0, booking: []};
    localStorage.setItem('searchParams', JSON.stringify(this.searchForm.convert()));
    this.location.go(this.router.createUrlTree(['ru/booking'], { queryParams: this.searchForm.convert() }).toString());
    this.bookingPostInfo = new BookingPost(this.searchForm, 0, this.tableLenght);
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.destroy();
      // this.dtOptions.pageLength = 15;
      this.dtTrigger.next();
    });
  }
  onSubmitEdition() {
    this.bookRoomsInfo.rooms.forEach((item, index) => {
      if (this.bookRoomsInfo.changed[index]) {
        const data = {
          from: moment(item.checkIn).format('YYYY-MM-DD'),
          to: moment(item.checkOut).format('YYYY-MM-DD'),
        };
        this.bookingservices.bookEditBookingRoom(this.bookRoomsInfo.id, item.id, data).subscribe(res => {
          this.notificate = new Notify('post', 'success');
          this.closeEdit();
        }, err => {
          this.notificate = new Notify('post', 'error');
        });
      }
    });
  }
  openFullInfo(id: number) {
    // this.currentBookingId = id;
    // // this.modalRef2.hide();
    // this.displayBookingInfo = true;
    this.router.navigate(['ru/booking', id]);
  }
}
