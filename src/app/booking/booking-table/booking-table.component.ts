import {Component, EventEmitter, Input, OnChanges, OnInit, Output} from '@angular/core';
import {BookingServices} from '../booking.services';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';

@Component({
  selector: 'booking-table',
  templateUrl: './booking-table.component.html',
  styleUrls: ['./booking-table.component.css']
})
export class BookingTableComponent implements OnInit, OnChanges {
  @Input() bookInfo: number;
  @Output() closeInfo = new EventEmitter<boolean>();
  info: any;
  page = false;

  constructor(private bookingservices: BookingServices, private activaroute: ActivatedRoute, private router: Router) {
    if (this.activaroute.snapshot.params.id) {
      this.page = true;
      this.bookingservices.getRoomsOfBooking(Number(this.activaroute.snapshot.params.id)).subscribe(res => {
        this.info = res;
        this.countFullPrice();
      });
    }
  }
  ngOnChanges() {
    if (this.bookInfo) {
      this.bookingservices.getRoomsOfBooking(this.bookInfo).subscribe(res => {
        window.scrollTo(0, 0);
        this.info = res;
        this.countFullPrice();
      });
    }
  }
  ngOnInit() {
  }
  close() {
    this.closeInfo.emit(false);
  }
  close2() {
    const params = JSON.parse(localStorage.getItem('searchParams'));
    console.log(params);
    this.router.navigate(['ru/booking'], { queryParams: params });
  }
  countFullPrice() {
    this.info['fullprice'] = 0;
    this.info.rooms.forEach((item, i) => {
      let fullroomprice = 0;
      item.days.forEach(it => {
        fullroomprice += it.price;
      });
      this.info.rooms[i]['fullroomprice'] = fullroomprice;
      this.info.fullprice += fullroomprice;
      if (item.nutritions[0]) {
        this.info.fullprice += item.nutritions[0].price;
      }
    });
  }
}
