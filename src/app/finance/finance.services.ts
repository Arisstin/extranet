import { Injectable } from '@angular/core';
import {Observable} from 'rxjs/Rx';
import {BaseService} from '../core/base.service';

@Injectable()
export class FinanceServices extends BaseService {
  getFinance(): Observable<any> {
    return this.get('hotel/finance');
  }
  downloadFile(id: number): Observable<Blob> {
    return this.get('report/' + id + '/exel');
  }
}
