import { Component, OnInit } from '@angular/core';
import { Globals } from '../globals';
import { Subject } from 'rxjs/Subject';
import { FinanceServices } from './finance.services';
import 'rxjs/Rx' ;

@Component({
  selector: 'finance',
  templateUrl: './finance.component.html',
  styleUrls: ['./finance.component.css']
})
export class FinanceComponent implements OnInit {
  loaded = false;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  backEnd: string;
  finance: any[];
  constructor(private globals: Globals, private financeservice: FinanceServices) {
    this.backEnd = this.globals.BACK_END;
    // this.dtTrigger.next();
  }
  downloadFile(id: number) {
    this.financeservice.downloadFile(id).subscribe(res => {
      console.log('res');
      const contentType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
      const blob = new Blob([(<any>res)], { type: contentType });
      const url = window.URL.createObjectURL(blob);
      window.open(url);
    }, err => console.log('error', err));
  }
  ngOnInit() {
    this.dtOptions = {
      pagingType: 'first_last_numbers',
      lengthMenu: [15, 25, 50, 75, 100],
      searching: false,
      // processing: true,
      ordering: false,
      // serverSide: false,
    };
    this.financeservice.getFinance().subscribe(res => {
      this.finance = res;
      console.log(res);
      this.loaded = true;
    }, err => {
      if (err.status === 404) {
        this.finance = [];
      }
      this.loaded = true;
    });
  }

}
