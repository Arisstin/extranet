import { Injectable } from '@angular/core';

Injectable()
export class Globals {
  // FRONT_END = 'http://bronirovka.ua';
  // BACK_END = 'http://api.bronirovka.ua/';
  // IMAGE_URL = 'http://images.bronirovka.ua/';
  FRONT_END = 'http://192.168.0.100:8000';
  BACK_END = 'http://localhost:8000/';           //'http://192.168.0.101:8000/';         'http://api.bronirovka.ua/';
  IMAGE_URL = 'http://images.bronirovka.ua/';
  HOTEL_LIMIT = 5;
  JOIN_HREF = 'http://localhost:4201/';
}
