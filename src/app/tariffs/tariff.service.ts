import { Injectable } from '@angular/core';
import {Observable} from 'rxjs/Rx';

import { BaseService } from '../core/base.service';
import { TariffDTO } from './tariff-dto';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class TariffService extends BaseService {
  public postTariff(tariff: TariffDTO): Observable<TariffDTO> {
    return this.post('tariffs/create', tariff);
  }
  public checkTariffName(data: string): Observable<any> {
    return this.post('tariffs/check-uiqueness', {title: data});
  }
}
