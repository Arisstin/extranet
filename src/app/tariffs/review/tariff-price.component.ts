import { Component, OnInit, Input, Output, EventEmitter} from '@angular/core';

import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { ReviewService } from './review.service';
import { EventService } from './event.service';

import * as moment from 'moment';
import {BsDatepickerConfig} from 'ngx-bootstrap';

@Component({
  selector: 'review',
  templateUrl: './tariff-price.component.html',
  styleUrls: ['./tariff-price.component.css']
})
export class TariffPriceComponent {

	@Input() tariff: any;
	@Input() guest: number;
	@Input() period: any;

  data: {[k: string]: any} = {};
  minDate = new Date();
  minDateF = new Date();
  bsConfig: Partial<BsDatepickerConfig>;
  colorTheme = 'theme-blue';
  isSuccess: boolean;


  @Output() change = new EventEmitter();

   constructor(
     public modalRef: BsModalRef,
     private reviewService: ReviewService,
     private eventService: EventService
     ) {
     this.bsConfig = Object.assign({}, {
       containerClass: this.colorTheme,
       showWeekNumbers: false,
       dateInputFormat: 'DD.MM.YYYY',
       displayMonths: 2
     });
   }
  dateChanged(value) {
    if (value !== null) {
      const day = new Date();
      day.setFullYear(value.getFullYear(), value.getMonth(), value.getDate() + 1);
      this.minDate = value;
      if (value > this.period[1]) {
        this.period[1] = day;
      }
    }
  }
   submitForm():void{
    this.data.period = this.reviewService.convertPeriod(this.period);
    this.data.tariffId = this.tariff.id;
    this.reviewService.postPriceForPeriod(this.tariff.id,this.data)
      .subscribe(
        data => this.onSuccessSending(),
        err => this.isSuccess = false
      );
   }
   onSuccessSending():void{
     this.isSuccess = true;
     this.modalRef.hide();
     this.eventService.priceForPeriodWasChanged();
   }
}
