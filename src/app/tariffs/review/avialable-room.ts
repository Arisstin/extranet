import * as moment from 'moment';

export class AvialableRoom{
	constructor(
		public id:number,
		public singleOccupa:number,
		public date: moment.Moment
		){}
}