import { Injectable } from '@angular/core';
import {Observable} from 'rxjs/Rx';

import { BaseService } from '../../core/base.service';
import { Room } from './room';
import { HttpParams } from '@angular/common/http';
import * as moment from 'moment';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class ReviewService extends BaseService {

	public getRoomsWithTariffs(period: any): Observable<any> {

		let params = new HttpParams()
			.set('from', period.from.format('YYYY-MM-DD'))
			.set('to', period.to.format('YYYY-MM-DD'));

		return this.getWithParams('rooms/review',{ params: params });
	}

	public postPriceForPeriod(id: number, data: any): Observable<any> {
		return this.post('tariffs/change-price-for-period', data);
	}
	public postAvailableForPeriod(id: number, data: any): Observable<any> {
	  return this.post('tariffs/' + id + '/close-sales-for-period', data);
  }
  postWorkPeriod(id: number, data: any): Observable<any> {
	  return this.post('tariffs/' + id + '/extend', data);
  }
  public postCountForPeriod(id: number, data: any): Observable<any> {
	  return this.post('rooms/' + id + '/change-amount-for-period', data);
  }
  public convertDate(day: Date) {
	  return moment(day).format('YYYY-MM-DD');
  }
	public convertPeriod(periodDate: any): any {
    const period: {[k: string]: any} = {};
    period.from = moment(periodDate[0]).format('YYYY-MM-DD');
    period.to = moment(periodDate[1]).format('YYYY-MM-DD');

    return period;
   }
  public changePrice(data: any): Observable<any> {
    return this.post('tariff-day/' + data.id + '/change-price', data);
  }
  public changeCount(data: any): Observable<any> {
	  return this.post('rooms-count/' + data.id + '/update', data);
	}
	public cancelDiscount(id: number): Observable<any> {
	  return this.post('discount/' + id + '/remove', {});
  }
  public postCloseForSales(id: number): Observable<any> {
	  return this.post(`tariff-day/${id}/close`, {});
	}
	public removeTariff(id: number): Observable<any> {
	  return this.post('tariffs/' + id + '/remove', {});
  }
}
