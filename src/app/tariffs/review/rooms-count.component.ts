import { Component, OnInit, Input, Output, EventEmitter} from '@angular/core';

import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { ReviewService } from './review.service';
import { EventService } from './event.service';

import * as moment from 'moment';
import {BsDatepickerConfig} from 'ngx-bootstrap';

@Component({
  selector: 'room-count',
  templateUrl: './rooms-count.component.html',
  styleUrls: ['./rooms-count.component.css']
})
export class RoomsCountComponent {
  @Input() room: any;
  @Input() period: any;

  data: {[k: string]: any} = {};
  isSuccess: boolean;
  minDate = new Date();
  minDateF = new Date();
  bsConfig: Partial<BsDatepickerConfig>;
  colorTheme = 'theme-blue';

  @Output() change = new EventEmitter();

  constructor(
    public modalRef: BsModalRef,
    private reviewService: ReviewService,
    private eventService: EventService
  ) {
    this.bsConfig = Object.assign({}, {
      containerClass: this.colorTheme,
      showWeekNumbers: false,
      dateInputFormat: 'DD.MM.YYYY',
      displayMonths: 2
    });
  }
  submitForm(): void {
    console.log(this.room);
    this.data.period = this.reviewService.convertPeriod(this.period);
    this.reviewService.postCountForPeriod(this.room.roomId, this.data)
      .subscribe(
        data => this.onSuccessSending(),
        err => this.onSuccessSending() // this.isSuccess = false
      );
  }
  dateChanged(value) {
    if (value !== null) {
      const day = new Date();
      day.setFullYear(value.getFullYear(), value.getMonth(), value.getDate() + 1);
      this.minDate = value;
      if (value > this.period[1]) {
        this.period[1] = day;
      }
    }
  }
  onSuccessSending(): void {
    this.isSuccess = true;
    this.modalRef.hide();
    this.eventService.priceForPeriodWasChanged();
  }
}
