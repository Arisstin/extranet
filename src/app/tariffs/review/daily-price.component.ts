import { Component, OnInit, Input } from '@angular/core';

import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { TariffService } from '../tariff.service';
import * as moment from 'moment';
import {EventService} from './event.service';
import {ReviewService} from './review.service';
import {BsDatepickerConfig} from 'ngx-bootstrap';

@Component({
  selector: 'review',
  templateUrl: './daily-price.component.html',
  styleUrls: ['./daily-price.component.css']
})
export class DailyPriceComponent {
  @Input() tariff: any;
  @Input() period: any;
  data: {[k: string]: any} = {};
  isSuccess: boolean;
  minDate = new Date();
  minDateF = new Date();
  bsConfig: Partial<BsDatepickerConfig>;
  colorTheme = 'theme-blue';

   constructor(
     public modalRef: BsModalRef,
     private reviewService: ReviewService,
     private eventService: EventService
   ) {
     this.bsConfig = Object.assign({}, {
       containerClass: this.colorTheme,
       showWeekNumbers: false,
       dateInputFormat: 'DD.MM.YYYY',
       displayMonths: 2
     });
   }

  submitForm(): void {
     this.data.action = 'OPEN';
    this.data.period = this.reviewService.convertPeriod(this.period);
    this.reviewService.postAvailableForPeriod(this.tariff.id, this.data)
      .subscribe(
        data => this.onSuccessSending(),
        err => this.isSuccess = false
      );
  }
  submitForm_1(): void {
    this.data.action = 'CLOSE';
    this.data.period = this.reviewService.convertPeriod(this.period);
    this.reviewService.postAvailableForPeriod(this.tariff.id, this.data)
      .subscribe(
        data => this.onSuccessSending(),
        err => this.isSuccess = false
      );
  }
  dateChanged(value) {
    if (value !== null) {
      const day = new Date();
      day.setFullYear(value.getFullYear(), value.getMonth(), value.getDate() + 1);
      this.minDate = value;
      if (value > this.period[1]) {
        this.period[1] = day;
      }
    }
  }
  onSuccessSending(): void {
    this.isSuccess = true;
    this.modalRef.hide();
    this.eventService.priceForPeriodWasChanged();
   }
}
