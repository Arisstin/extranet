import { Tariff } from './tariff';

export class Room{
	constructor(
		public id:number,
		public title:any, 
		public tariffs: Tariff[],
		){}
}