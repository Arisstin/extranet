import { Component, OnInit } from '@angular/core';

import { ReviewService } from './review.service';
import { EventService } from './event.service';

import { Room } from './room';
import {Router, ActivatedRoute, Params} from '@angular/router';
import * as moment from 'moment';
import { BsDatepickerConfig } from 'ngx-bootstrap';

@Component({
  selector: 'review',
  templateUrl: './review.component.html',
  styleUrls: ['./review.component.css']
})
export class ReviewComponent implements OnInit {
  static readonly DAYS_TO_SHOW = 31;
  date: Date = new Date();
  period: {[k: string]: any} = {};
  loaded = false;
  rooms: any;
  bsConfig: Partial<BsDatepickerConfig>;
  colorTheme = 'theme-blue';
  constructor(
    private reviewService: ReviewService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private eventService: EventService
  ) {
    this.bsConfig = Object.assign({}, {
      containerClass: this.colorTheme,
      showWeekNumbers: false,
      dateInputFormat: 'dd, D MMMM YYYY',
      displayMonths: 2
    });
  }
  ngOnInit(): void {
    this.initPeriod();
    this.eventService.priceForPeriodWasChangedEvent
      .subscribe(isSuccess => { this.laodRooms(); });
  }
  show(): void {
    this.router.navigate(['/ru/tariffs/review'], {
      queryParams: {
        from: moment(this.date).format('YYYY-MM-DD')
      }});
  }
  initPeriod(): void {
    this.activatedRoute.queryParams
      .subscribe(params => {
        if (params.from) {
          this.period.from = moment(this.date);
          this.period.to = moment(this.date).add(ReviewComponent.DAYS_TO_SHOW, 'days');
        } else {
          this.date = new Date();
          this.period.from = moment(this.date);
          this.period.to = moment(this.date).add(ReviewComponent.DAYS_TO_SHOW, 'days');
          this.show();
        }
        this.laodRooms();
      });
  }
  laodRooms(): void {
    this.reviewService
      .getRoomsWithTariffs(this.period)
      .subscribe(rooms => {
        this.loaded = true;
        this.rooms = rooms;
      });
  }
}
