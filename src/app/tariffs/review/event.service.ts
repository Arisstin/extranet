import { Injectable, Output, EventEmitter } from '@angular/core';

@Injectable()
export class EventService {

  isSuccess = false;

  @Output() priceForPeriodWasChangedEvent: EventEmitter<boolean> = new EventEmitter();

  priceForPeriodWasChanged() {
  	this.isSuccess = true;
  	this.priceForPeriodWasChangedEvent.emit(this.isSuccess);
  }
}
