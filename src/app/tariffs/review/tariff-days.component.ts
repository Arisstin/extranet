import {Component, Input} from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import {EventService} from './event.service';
import {ReviewService} from './review.service';
import {BsDatepickerConfig} from 'ngx-bootstrap';

@Component({
  selector: 'tariff-days',
  templateUrl: './tariff-days.component.html',
  styleUrls: ['./tariff-days.component.css']
})
export class TariffDaysComponent {
  @Input() tariff: any;
  @Input() from: Date;
  data: {[k: string]: any} = {};
  day: Date;
  isSuccess: boolean;
  bsConfig: Partial<BsDatepickerConfig>;
  colorTheme = 'theme-blue';

  constructor(
    public modalRef: BsModalRef,
    private reviewService: ReviewService,
    private eventService: EventService
  ) {
    this.bsConfig = Object.assign({}, {
      containerClass: this.colorTheme,
      showWeekNumbers: false,
      dateInputFormat: 'DD.MM.YYYY',
      displayMonths: 2
    });
  }
  submitForm(): void {
    if (this.day !== undefined) {
      this.data.to = this.reviewService.convertDate(this.day);
      this.reviewService.postWorkPeriod(this.tariff.id, this.data)
        .subscribe(
          data => this.onSuccessSending(),
          err => this.isSuccess = false
        );
    }
  }
  onSuccessSending(): void {
    this.isSuccess = true;
    this.modalRef.hide();
    this.eventService.priceForPeriodWasChanged();
  }
}
