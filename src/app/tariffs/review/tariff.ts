import { AvialableRoom } from './avialable-room';

export class Tariff {
	constructor(
		public id: number,
		public aliase: string,
		public day: AvialableRoom[],
    public from: string,
    public to: string
		){}
}
export class CancelConfirm {
  roomind: number;
  tariffind: number;
  id: number;
  public constructor(id: number, tariff: number, room: number) {
    this.id = id;
    this.tariffind = tariff;
    this.roomind = room;
  }
}
