import {Component, OnInit, Input, OnChanges, SimpleChanges, Renderer2, ViewChild, ElementRef} from '@angular/core';
import * as moment from 'moment';
import { extendMoment, MomentRangeExtends, DateRange } from 'moment-range';

import { BsModalService } from 'ngx-bootstrap';
import { TariffPriceComponent } from './tariff-price.component';
import { DailyPriceComponent } from './daily-price.component';
import { ReviewService } from './review.service';
import {RoomsCountComponent} from './rooms-count.component';
import {forEach} from '@angular/router/src/utils/collection';
import {TariffDaysComponent} from './tariff-days.component';
import {CancelConfirm} from './tariff';

@Component({
  selector: 'review-table',
  templateUrl: './review-table.component.html',
  styleUrls: ['./review-table.component.css']
})
export class ReviewTableComponent implements OnInit, OnChanges {
  @Input() rooms: any;
  @Input() from: Date;
  @Input() period: {from: moment.Moment, to: moment.Moment};
  @ViewChild('table') tableReview: ElementRef;
  @ViewChild('daystable') daysTable: ElementRef;
  header;
  days;
  data = [];
  loading: boolean;
  daysWidth = Array(32).fill(30);
  secondHeaderOffset = 0;
  displaySecondHead = false;
  removeConfirm: CancelConfirm;
  constructor(
    private modalService: BsModalService,
    private reviewService: ReviewService,
    private renderer: Renderer2,
    ) {}

  ngOnInit(): void {}

  ngOnChanges(changes: SimpleChanges) {
    const t0 = performance.now();
    this.prepareData();
    console.log(changes);
    const t1 = performance.now();
    console.log('Call to doSomething took ' + (t1 - t0) + ' milliseconds.');
  }
  prepareData(): void {
    this.data = [];
    this.days = [];
    this.header = [];
    const start = this.period.from;
    const end = this.period.to;
    const range = extendMoment(moment).range(start, end);
    const days = Array.from(range.by('days'));
    this.days = days;
    const months: Map<string, moment.Moment[]> = new Map<string, moment.Moment[]>();
    for (const day of this.days ) {
      if (!months.has(day.format('MMMM'))) {
        months.set(day.format('MMMM'), []);
      }
      months.get(day.format('MMMM')).push(day);
    }
    months.forEach((value, key, map) => {
      this.header.push({month: value[0], count: value.length});
    });
    for (const room of this.rooms) {
      const roomData: {[k: string]: any} = {};
      roomData.roomId = room.id;
      roomData.guest = Array(room.guest).fill(1);
      roomData.title = room.title.title;
      roomData.dailyData = new Array();
      // room count ///
      const roomCountsMap = new Map<string, any>();
      for (const rc of room.dailyCount) {
        roomCountsMap.set(rc.date, rc);
      }
      for (const day of this.days) {
        const rcData: {[k: string]: any} = {};
        const date = day.format('YYYY-MM-DD');
        if (roomCountsMap.has(date)) {
          rcData.id = roomCountsMap.get(date).id;
          rcData.count = roomCountsMap.get(date).count;
          rcData.isNew = false;
        } else {
          rcData.isNew = true;
        }
        roomData.dailyData.push(rcData);
      }
      // end room count //
      roomData.tariffs = [];
      // room count ///
      for (const tariff of room.tariffs) {
        const tariffData: {[k: string]: any} = {};
        tariffData.id = tariff.id;
        tariffData.title = tariff.title;
        tariffData.to = tariff.to;
        tariffData.from = tariff.from;
        tariffData.discounts = this.sortTariffs(tariff.discounts);
        tariffData.room = room;
        tariffData.ra = [];
        const raMap = new Map<string, any>();
        for (const ra of tariff.roomAvailabls) {
          raMap.set(ra.date, ra);
        }
        for (const day of this.days) {
          const raData: {[k: string]: any} = {};
          const date = day.format('YYYY-MM-DD');
          if (raMap.has(date)) {
            raData.id = raMap.get(date).id;
            raData.isAvailabl = raMap.get(date).isAvailabl;
            raData.singleOccupancyPrice = raMap.get(date).singleOccupancyPrice;
            raData.doubleOccupancyPrice = raMap.get(date).doubleOccupancyPrice;
            raData.tripleOccupancyPricePercent = raMap.get(date).tripleOccupancyPricePercent;
            raData.fourOccupancyPricePercent = raMap.get(date).fourOccupancyPricePercent;
            raData.fiveOccupancyPricePercent = raMap.get(date).fiveOccupancyPricePercent;
            raData.discounts = [];
            tariff.discounts.forEach(item => {
              raData.discounts.push(this.findDiscount(item.id, raMap.get(date).discounts));
            });
            raData.isNew = false;
          } else {
            raData.id = null;
            raData.isNew = true;
          }
          tariffData.ra.push(raData);
        }
        roomData.tariffs.push(tariffData);
      }
      this.data.push(roomData);
    }
  }
  changePriceForPeriod($event: any, tariff: any, guest: number): void {
    const bsModalRef = this.modalService.show(TariffPriceComponent, {class: 'modal-sm'});
    bsModalRef.content.guest = guest;
    bsModalRef.content.tariff = tariff;
    const dayto = this.period.from.toDate();
    dayto.setDate(dayto.getDate() + 1);
    bsModalRef.content.period = [this.period.from.toDate(), dayto];
  }
  changeCountForPeriod(room: any) {
    const bsModalRef = this.modalService.show(RoomsCountComponent, {class: 'modal-sm'});
    const dayto = this.period.from.toDate();
    dayto.setDate(dayto.getDate() + 1);
    bsModalRef.content.period = [this.period.from.toDate(), dayto];
    bsModalRef.content.room = room;
  }
  changeAvailableForPeriod(tariff: any) {
    const bsModalRef = this.modalService.show(DailyPriceComponent, {class: 'modal-sm'});
    const dayto = this.period.from.toDate();
    dayto.setDate(dayto.getDate() + 1);
    bsModalRef.content.period = [this.period.from.toDate(), dayto];
    bsModalRef.content.tariff = tariff;
  }
  cancelDiscount(roomindex: number, tariffindex: number, discountindex: number, id: number) {
    this.reviewService.cancelDiscount(id).subscribe(res => {
      this.data[roomindex].tariffs[tariffindex].discounts.splice(discountindex, 1);
    });
  }
  checkCharCount(event, day: any) {
    if (event.keyCode === 13) {
      this.modifyCount(day);
    }
    if (event.keyCode < 47 || event.keyCode > 58) {
      event.preventDefault();
    }
  }
  checkChar(event, day: any) {
    if (event.keyCode === 13) {
      this.savePrice(day);
    }
    if (event.keyCode !== 46 && (event.keyCode < 47 || event.keyCode > 58)) {
      event.preventDefault();
    }
  }
  daySize(event) {
    console.log(event);
  }
  increaseWorkDays(tariff: any) {
    const dayfrom = new Date(tariff.to);
    dayfrom.setDate(dayfrom.getDate() + 1);
    console.log(tariff, dayfrom);
    const bsModalRef = this.modalService.show(TariffDaysComponent, {class: 'modal-sm'});
    bsModalRef.content.from = dayfrom;
    bsModalRef.content.tariff = tariff;
  }
  tableScroll() {
    if (this.tableReview.nativeElement.getBoundingClientRect().top < 0) {
      this.secondHeaderOffset = Math.abs(this.tableReview.nativeElement.getBoundingClientRect().top + 3);
      this.displaySecondHead = true;
    } else {
      this.displaySecondHead = false;
    }
    this.setDaysWidth();
  }
  discountClass(index: number, type: string, data: any[]): string {
    const percent = data[index].percent;
    let less = false;
    data.forEach(item => {
      if (item.percent && item.type === type && item.percent > percent) {
        less = true;
      }
    });
    return less ? 'less' : '';
  }
  getLenghtforStyle(value: number) {
    if (value) {
      return value.toString().length;
    } else {
      return 3;
    }
  }
  findDiscount(id: number, data: any[]) {
    const res = data.find(x => x.tariff.id === id);
    return res ? res : {'percent': undefined};
  }
  closeForSales($event: any, index: number, day: any): void {
    if (day.id !== null) {
      this.reviewService.postCloseForSales(day.id).subscribe(ra => {
        console.log(this.getClass(ra));
        day.isAvailabl = !day.isAvailabl;
        this.renderer.removeClass($event.target, this.getClass(day));
        this.renderer.addClass($event.target, this.getClass(ra));
      });
    }
  }
  getClass(day: any, index?: number, count?: number): string {
    if (day.isNew) {
      return 'no-data';
    }
    if (day.isAvailabl) {
      if (Number(count) === 0) {
        return 'zeros';
      } else {
        return 'available';
      }
    } else {
      return 'closed';
    }
  }
  getDiscountClass(value: string) {
    switch (value) {
      case 'BASIC' : {
        return 'basediscount';
      }
      case 'MIN_STAY' : {
        return 'minstaydiscount';
      }
      case 'LAST_MINUTE' : {
        return 'lastmindiscount';
      }
    }
  }
  modifyCount(day: any) {
    this.reviewService.changeCount(day).subscribe();
  }
  removeTariff(id: number, tarifind: number, roomind: number) {
    this.removeConfirm = new CancelConfirm(id, tarifind, roomind);
  }
  removeTarifff() {
    this.reviewService.removeTariff(this.removeConfirm.id).subscribe(res => {
      this.data[this.removeConfirm.roomind].tariffs.splice(this.removeConfirm.tariffind, 1);
      if (this.data[this.removeConfirm.roomind].tariffs.length === 0) {
        this.data.splice(this.removeConfirm.roomind, 1);
      }
    });
  }
  savePrice(day: any) {
    this.reviewService.changePrice(day).subscribe();
  }
  setDaysWidth() {
    this.daysWidth[32] = this.daysTable.nativeElement.children[0].offsetWidth;
    for (let i = 0; i < 32; i++) {
      this.daysWidth[i] = this.daysTable.nativeElement.children[i + 1].offsetWidth;
    }
  }
  setDayWidth(index: number) {
    this.daysWidth[index] = this.daysTable.nativeElement.children[index + 1].offsetWidth;
  }
  showDiscount(index: number, data: any[]) {
    let show = false;
      data.forEach(item => {
        if (item.discounts && item.discounts[index].type) {
          show = true;
        }
      });
    return show;
  }
  sortTariffs(tariffs: any[]) {
    tariffs.sort(function (value1, value2) {
      switch (value1.type) {
        case 'BASIC' : {
          return value2.type === 'BASIC' ? 0 : -1;
        }
        case 'MIN_STAY' : {
          if (value2.type === 'LAST_MINUTE') {
            return -1;
          } else {
            return value2.type === 'BASIC' ? 1 : 0;
          }
        }
        case 'LAST_MINUTE' : {
          return value2.type === 'LAST_MINUTE' ? 0 : 1;
        }
        default : {
          return 1;
        }
      }
    });
    return tariffs;
  }
  unshowTariff(data: any[]) {
    let unshow = true;
    data.forEach(item => {
      if (!item.isNew) {
        unshow = false;
      }
    });
    return unshow;
  }
}
