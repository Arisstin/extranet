import { Component, OnInit } from '@angular/core';

import { RoomService } from '../website/rooms/room.service';
import { TariffService } from './tariff.service';

import { Room } from '../website/rooms/room';
import { TariffDTO } from './tariff-dto';
import { Nutrition } from './nutrition';
import { BsDatepickerConfig } from 'ngx-bootstrap';
import * as moment from 'moment';
import {Notify} from '../widgets/notification/notification.classes';

@Component({
  selector: 'add-tariff',
  templateUrl: './add-tariff.component.html',
  styleUrls: ['./add-tariff.component.css']
})
export class AddTariffComponent implements OnInit {
  weekdays = ['mo', 'tu', 'we', 'th', 'fr', 'sa', 'su'];
  bsConfig: Partial<BsDatepickerConfig>;
  colorTheme = 'theme-dark-blue';
  DataValue: Date[] = [new Date(), new Date()];
  MinDates: Date[] = [new Date(), new Date()];
  rooms: Room[];
  tariff: TariffDTO;
  nutritionType: string;
  breackfastInc: boolean;
  halfBoardInc: boolean;
  fullBoardInc: boolean;
  breackfast: Nutrition;
  halfBoard: Nutrition;
  fullBoard: Nutrition;
  tariffUniq = true;
  loaded = false;
  emptyRooms = false;
  notificate: any;
  constructor(
    private roomService: RoomService,
    private tariffService: TariffService
  ) {
    this.DataValue[1].setDate(this.DataValue[0].getDate() + 1);
    this.MinDates[1] = this.DataValue[1];
    this.bsConfig = Object.assign({}, {
      containerClass: this.colorTheme,
      showWeekNumbers: false,
      dateInputFormat: 'dd, D MMMM YYYY',
      displayMonths: 2
    });
    this.tariff = new TariffDTO();
    this.tariff.weekdays = [];
    this.breackfastInc = false;
    this.halfBoardInc  = false;
    this.fullBoardInc  = false;
    this.initNutrition();
  }
  ngOnInit(): void {
    this.roomService.getRooms().subscribe(rooms => {
      this.rooms = rooms;
      this.loaded = true;
    }, err => {
      this.loaded = true;
      if (err.status === 404) {
        this.emptyRooms = true;
      }
    });
  }
  dateChanged(value) {
    if (value !== null) {
      const day = new Date();
      day.setFullYear(value.getFullYear(), value.getMonth(), value.getDate() + 1);
      this.MinDates[1] = day;
      if (value > this.DataValue[1] || (value.getDate() >= this.DataValue[1].getDate() && value.getMonth() >= this.DataValue[1].getMonth() && value.getFullYear() >= this.DataValue[1].getFullYear())) {
        this.DataValue[1] = day;
      }
    }
  }
  onDayChange(event, day: number): void {
    if (event.target.checked) {
      this.tariff.weekdays.push(day);
    } else {
      const index = this.tariff.weekdays.indexOf(day);
      if (index > -1) {
        this.tariff.weekdays.splice(index, 1);
      }
    }
  }
  setAllDays(event): void {
    event.preventDefault();
    this.tariff.weekdays = [1, 2, 3, 4, 5, 6, 7];
  }
  desellectAllDays(ebvent): void {
    event.preventDefault();
    this.tariff.weekdays = [];
  }
  checkTariffName() {
    this.tariffService.checkTariffName(this.tariff.title).subscribe(res => {
      this.tariffUniq = res.isUnique;
    });
  }
  addNutrition(): void {
    this.tariff.nutrition = [];
    if (this.nutritionType === 'CHARGEABLE') {
      if (this.breackfastInc) { this.tariff.nutrition.push(this.breackfast); }
      if (this.halfBoardInc) { this.tariff.nutrition.push( this.halfBoard); }
      if (this.fullBoardInc) { this.tariff.nutrition.push(this.fullBoard); }
    } else {
      const nut = new Nutrition();
      nut.type = this.nutritionType;
      this.tariff.nutrition.push(nut);
    }
  }
  toDate(date: any): string {
    if (date !== null) {
      return  moment(date).format('YYYY-MM-DD');
    }
  }
  onSubmit(): void {
    // console.log(this.toDate(this.tariff.from));
    this.addNutrition();
    this.tariff.from = this.toDate(this.DataValue[0]);
    this.tariff.to = this.toDate(this.DataValue[1]);
    console.log(this.tariff);
    this.tariffService.postTariff(this.tariff).subscribe(
      tariff => this.notificate = new Notify('post', 'success'),
      err => this.notificate = new Notify('post', 'error')
      );
  }
  getGuestById(id: number): number {
    for (const room of this.rooms ){
      if (room.id === Number(id)) {
        return room.guest;
      }
    }
    return 0;
  }
  saveAvailable() {
    return !this.rooms || !this.nutritionType || !this.tariffUniq || !this.tariff.roomId || this.tariff.weekdays.length === 0 || !this.tariff.type || !this.tariff.title || this.tariff.title === '' || !this.checkPrice();
  }
  checkPrice() {
    const count = this.getGuestById(this.tariff.roomId);
    switch (count) {
      case 1 : { return this.tariff.singleOccupancyPrice && this.tariff.singleOccupancyPrice > 0; }
      case 2 : { return (this.tariff.singleOccupancyPrice && this.tariff.singleOccupancyPrice > 0) || (this.tariff.doubleOccupancyPrice && this.tariff.doubleOccupancyPrice > 0); }
      case 3 : { return (this.tariff.singleOccupancyPrice && this.tariff.singleOccupancyPrice > 0) || (this.tariff.doubleOccupancyPrice && this.tariff.doubleOccupancyPrice > 0) || (this.tariff.tripleOccupancyPricePercent && this.tariff.tripleOccupancyPricePercent > 0); }
      case 4 : {
        return (this.tariff.singleOccupancyPrice && this.tariff.singleOccupancyPrice > 0) ||
          (this.tariff.doubleOccupancyPrice && this.tariff.doubleOccupancyPrice > 0) ||
          (this.tariff.tripleOccupancyPricePercent && this.tariff.tripleOccupancyPricePercent > 0) ||
          (this.tariff.fourOccupancyPricePercent && this.tariff.fourOccupancyPricePercent > 0); }
      case 5 : {
        return (this.tariff.singleOccupancyPrice && this.tariff.singleOccupancyPrice > 0) ||
          (this.tariff.doubleOccupancyPrice && this.tariff.doubleOccupancyPrice > 0) ||
          (this.tariff.tripleOccupancyPricePercent && this.tariff.tripleOccupancyPricePercent > 0) ||
          (this.tariff.fourOccupancyPricePercent && this.tariff.fourOccupancyPricePercent > 0) ||
          (this.tariff.fiveOccupancyPricePercent && this.tariff.fiveOccupancyPricePercent); }
    }
  }
  initCheckboxDay(day: number): boolean {
    return this.tariff.weekdays.indexOf(day) > -1 ? true : false;
  }
  initNutrition(): void {
    this.breackfast = new Nutrition();
    this.halfBoard = new Nutrition();
    this.fullBoard = new Nutrition();
    this.breackfast.type = 'BREAKFAST';
    this.halfBoard.type = 'HALF_BOARD';
    this.fullBoard.type = 'FULL_BOARD';
  }
}
