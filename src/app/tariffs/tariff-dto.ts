import { Nutrition } from './nutrition';

export class TariffDTO{
	title: string;
	from: any;
	to: any;
	weekdays: number[];
	type: string;
	roomId: number;
	nutrition: Nutrition[];
	singleOccupancyPrice: number;
	doubleOccupancyPrice: number;
	tripleOccupancyPricePercent: number;
	fourOccupancyPricePercent: number;
	fiveOccupancyPricePercent: number;
}
