import {Component, AfterViewInit, Input, TemplateRef, ViewChild} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { Location } from '@angular/common';

import {AuthService} from './auth.service';
import {LoginForm} from './LoginForm';
import {LoginService} from './login.service';
import {AuthModalFormComponent} from './auth-modal-form.component';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import {Router} from '@angular/router';



@Component({
  selector: 'auth-modal',
  templateUrl: './auth-modal.component.html',
  styleUrls: ['./auth-modal.component.css']
})
export class AuthModalComponent implements AfterViewInit{

  modalRef: BsModalRef;
  openedForm: string;
  currentUrl: string;
  loginForm: LoginForm = new LoginForm;
  adminView = false;
  adminToken: string;
  errorMessage = false;
  @ViewChild(ModalDirective) modal: ModalDirective;

  constructor(
    private modalService: BsModalService,
    private authService: AuthService,
    private loginService: LoginService,
    private location: Location,
    private router: Router
    ) {
    if (this.router.url.split('?')[0] !== '/ru/login') {
      this.openedForm = localStorage.getItem('openlogin');
      this.authService.isAuthorized
        .subscribe(isAuthorized => {
          console.log('auth', isAuthorized);
          if (!isAuthorized) {
          if (!this.openedForm) {
            localStorage.setItem('openlogin', 'open');
            this.open();
          }
        }});
    }
  }

  open() {
    if (this.router.url.split('?')[0] !== '/ru/login') {
      this.modal.show();
    }
  }
  handler() {
    console.log('hide');
    localStorage.removeItem('openlogin');
  }

  ngAfterViewInit(): void {
    if (this.router.url.split('?')[0] !== '/ru/login') {
    this.openedForm = localStorage.getItem('openlogin');
      this.authService.isAuthorized
        .subscribe(isAuthorized => {if (!isAuthorized) {
          if (!this.openedForm) {
            localStorage.setItem('openlogin', 'open');
            this.open();
          }
        }});
    }
  }
  submitAdminForm() {
    this.loginService.loginAdmin(this.adminToken);
  }
   submitLoginForm() {
    this.errorMessage = false;
    localStorage.removeItem('openlogin');
    this.loginService.login(this.loginForm).subscribe(res => {
      this.loginService.log(res, false);
    }, err => {
      if (err.status === 401) {
        this.errorMessage = true;
      }
    });
  }
}
