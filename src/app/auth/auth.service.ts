import { Injectable,  } from '@angular/core';
import {HttpClient} from '@angular/common/http'
import {BehaviorSubject} from 'rxjs/Rx';
import { HttpRequest } from '@angular/common/http';

import {LoginForm} from './LoginForm';

@Injectable()
export class AuthService {

  isAuthorized:BehaviorSubject<boolean> = new BehaviorSubject<boolean>(true);

  cachedRequests: Array<HttpRequest<any>> = [];

  public getToken():string{
  	return localStorage.getItem('jwt-token');
  }

  public collectFailedRequest(request): void {
    this.cachedRequests.push(request);
  }

	public retryFailedRequests(): void {
    // retry the requests. this method can
    // be called after the token is refreshed
    this.cachedRequests.forEach(r => r.clone())
  }

}

