import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { LoginForm, ForgotPassword } from '../LoginForm';
import { LoginService } from '../login.service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { ActivatedRoute } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-auth-page',
  templateUrl: './auth-page.component.html',
  styleUrls: ['./auth-page.component.css']
})
export class AuthPageComponent implements OnInit {
  @ViewChild ('forgot2') forgot_2: TemplateRef<any>;
  adminView = false;
  adminToken: string;
  adminEnter = false;
  loginForm: LoginForm;
  forgotPasswordForm: FormGroup;
  formForgotPass1: BsModalRef;
  formForgotPass2: BsModalRef;
  passwordsConfirm: boolean;
  errorToken = false;
  Email: string;
  EmailSent: boolean;
  errorMessage = false;
  errorText: string;
  errorEmail = false;
  emailPattern = '^[a-z0-9._%+-]+@[a-z0-9.-]{1,}\.[a-z]{2,4}$';
  constructor(private loginService: LoginService, private modalService: BsModalService, private activeroute: ActivatedRoute) {
    this.loginForm = new LoginForm();
    this.passwordsConfirm = true;
    if (this.activeroute.snapshot.params.id) {
        localStorage.removeItem('jwt-token');
        localStorage.removeItem('hotelId');
        this.adminEnter = true;
        this.adminView = true;
    }
  }
  ngOnInit() {
    this.forgotPasswordForm = new FormGroup({
      'token': new FormControl('', Validators.required),
      'password_f': new FormControl('', [Validators.required, Validators.minLength(6)]),
      'passwordconfirm': new FormControl('', [Validators.required, Validators.minLength(6)]),
    }, {validators: this.passwordsEqual('password_f', 'passwordconfirm')});

    this.activeroute.queryParams.subscribe(res => {
      console.log(res);
      if (res.token) {
        this.forgotPasswordForm.patchValue({
          'token': res.token });
        this.formForgotPass2 = this.modalService.show(this.forgot_2, {class: 'second-forgot-modal'});
      }
    });
  }
  get token() { return this.forgotPasswordForm.get('token'); }
  get password_f() { return this.forgotPasswordForm.get('password_f'); }
  get passwordconfirm() { return this.forgotPasswordForm.get('passwordconfirm'); }
  submitAdminForm() {
    this.loginService.loginAdminMain(this.adminToken);
  }
  submitLoginForm() {
    this.errorMessage = false;
    this.loginService.login(this.loginForm).subscribe(res => {
      this.loginService.log(res, true);
    }, err => {
      if (err.status === 401) {
        this.errorMessage = true;
        this.errorText = 'Не правильные логин или пароль';
      }
      if (err.status === 500) {
        this.errorMessage = true;
        this.errorText = 'Ошибка сервера, повторите попытку';
      }
    });
  }
  openModalForgot1(template: TemplateRef<any>) {
    this.EmailSent = false;
    this.formForgotPass1 = this.modalService.show(template, {class: 'first-forgot-modal'});
  }
  openModalForgot2(template: TemplateRef<any>) {
    this.errorEmail = false;
    this.loginService.sendEmailForgotPassword(this.Email).subscribe(res => {this.errorEmail = false; this.EmailSent = true; }, err => {
      if (err.status === 404) {
        this.errorEmail = true;
      }
    });
  }
  sendEmailAgain() {
    this.loginService.sendEmailForgotPassword(this.Email);
  }
  passwordsEqual(passwordKey: string, confirmPasswordKey: string) {
    return (group: FormGroup): {
      [key: string]: any
    } => {
      const pass = group.controls[passwordKey];
      const confirm = group.controls[confirmPasswordKey];

      if (pass.value !== confirm.value) {
        return {
          mismatchedPasswords: true
        };
      }
    };
  }
  setNewPassword() {
    if (this.forgotPasswordForm.valid) {
      const forgot = new ForgotPassword(this.forgotPasswordForm.value);
      this.loginService.recoveryPassword(forgot).subscribe(res => {
        this.errorToken = false;
        this.formForgotPass2.hide();
        },
          err => {
        this.errorToken = true;
          });
    } else {
      this.passwordsConfirm = false;
    }
  }
}
