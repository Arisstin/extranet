export class LoginForm{
	login: string;
	password: string;
	constructor() {
	  this.login = '';
	  this.password = '';
  }
}
export class ForgotPassword {
  token: string;
  password: string;
  passwordconfirm: string;
  constructor(value: any) {
    this.token = value.token;
    this.password = value.password_f;
    this.passwordconfirm = value.passwordconfirm;
  }
}
