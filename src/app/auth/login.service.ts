import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { Globals } from '../globals';
import { Router } from '@angular/router';
import {ForgotPassword, LoginForm} from './LoginForm';
import {Token} from './token';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class LoginService {
  private tokenUrl: string;

  constructor(private http: HttpClient, private globals: Globals, private router: Router) {
    this.tokenUrl = this.globals.BACK_END + 'en/extranet/token';
  }
  public login(form: LoginForm): Observable<any> {
   return this.http.post<Token>(this.tokenUrl, form);
  }
  public loginAdmin(value: string) {
    this.setToken(value);
    localStorage.setItem('admin', 'yes');
    location.reload();
  }
  public loginAdminMain(value: string) {
    this.setToken(value);
    localStorage.setItem('admin', 'yes');
    this.router.navigate( ['/ru/general/hotel']);
    location.reload();
  }
  public log(res: any, relocate: boolean) {
    localStorage.removeItem('admin');
    localStorage.setItem('hotelId', res.hotelId.toString());
    this.setToken(res.token);
    if (relocate) { this.router.navigate( ['/ru/booking']); }
    location.reload();
  }
  public recoveryPassword(value: ForgotPassword) {
    return this.http.post<any>(this.globals.BACK_END + 'ru/extranet/recovery', value);
  }
  public sendEmailForgotPassword(value: string): Observable<any> {
    const email = {email: value};
    return this.http.post<any>(this.globals.BACK_END + 'ru/extranet/recovery/email', email);
  }
  public removeToken(): void {
     localStorage.removeItem('jwt-token');
     localStorage.removeItem('hotelId');
  }

  public setToken(token: string) {
    localStorage.setItem('jwt-token', token);
  }


}
