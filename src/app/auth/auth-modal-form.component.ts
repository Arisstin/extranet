import {Component, OnInit, Input} from '@angular/core';

import {LoginForm} from './LoginForm';
import {LoginService} from './login.service'

@Component({
  selector: 'ngbd-modal-content',
  templateUrl:'./auth-modal.component.html',
  styleUrls: ['./auth-modal.component.css']
})
export class AuthModalFormComponent {

  loginForm: LoginForm;
  adminView = false;
  adminToken: string;
  constructor(private loginService: LoginService){
    this.loginForm = new LoginForm();
  }
  handler() {
    console.log('hide');
    localStorage.removeItem('openlogin');
  }
  submitLoginForm(){
    this.loginService.login(this.loginForm);
  }
}
