import { Component } from '@angular/core'

@Component({
	selector:'photo',
	templateUrl: 'photo.component.html',
  styleUrls: ['./photo.component.css']
})
export class PhotoComponent {
  update: boolean;
  constructor() {
    this.update = false;
  }
  startUpdate($event) {
    this.update = $event;
  }
}
