import { Injectable }    from '@angular/core';
import { Headers, Response} from '@angular/http';
import { HttpClient } from '@angular/common/http';
import {Observable} from 'rxjs/Rx';
import {BehaviorSubject} from 'rxjs/Rx';

import { Photo } from './photo';
import { BaseService } from '../../core/base.service';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class PhotoService extends BaseService {

	public photos: BehaviorSubject<Photo[]> = new BehaviorSubject<Photo[]>([]);

	addNewPhoto(photo: Photo): void {
		this.photos.getValue().push(photo);
	}
	getPhotos(): Observable<Photo[]> {
		return this.get('photos');
	}
	deleteGalleryPhoto(id: number): Observable<any>  {
	  return this.post('photos/' + id + '/remove', null);
  }
	setPhotoAvatar(id: number): Observable<any>  {
    return this.post('photos/' + id + '/avatar', null);
  }
  deleteRoomPhoto(id: number, roomid: number): Observable<any> {
	  return this.post('remove-photo/' + id + '/room/' + roomid, null);
  }
  addRoomPhoto(id: number, roomid: number): Observable<any>  {
    return this.post('add-photo/' + id + '/room/' + roomid, null);
  }
}
