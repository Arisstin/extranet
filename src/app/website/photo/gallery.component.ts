import {Component, OnInit, Input, OnChanges, SimpleChanges} from '@angular/core';
import { PhotoService } from './photo.service';
import { Photo } from './photo';
import { RoomService } from '../rooms/room.service';
import {Room} from '../rooms/room';
import { Globals } from '../../globals';
import {Token} from '../../auth/token';

@Component({
	selector: 'gallery',
	templateUrl: 'gallery.component.html',
  styleUrls: ['./gallery.component.css']
})
export class GalleryComponent implements OnInit, OnChanges {
  @Input() update: boolean;
  photos: Photo[];
  loaded = false;
  photosToShow: Photo[];
  rooms: Room[];
  activeTab: number;
  dragactive = -1;
  showImages: string;
  currentPaginationPage: number;
  imgURL: string;
  hotelId: string;
  constructor(private photoService: PhotoService, private roomService: RoomService, private globals: Globals) {
    this.photosToShow = [];
    this.showImages = '15';
    this.currentPaginationPage = 1;
    this.imgURL = this.globals.IMAGE_URL;
    this.hotelId = localStorage.getItem('hotelId');
  }
  ngOnInit() {
    this.activeTab = 0;
    this.roomService.getRooms().subscribe(rooms => {
      this.rooms = rooms;
      this.loaded = true;
    }, err => this.loaded = true);
    this.photoService.getPhotos().subscribe(photos => {
      this.photos = photos;
      this.photos.forEach((_, index) => {
        this.photos[index]['checked'] = false;
      });
    });
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.update.previousValue !== undefined) {
      console.log(this.update);   // обновление галереи
      this.photoService.getPhotos().subscribe(photos => {
        this.photos = photos;
      });
    }
  }
  allowDrop(ev, index?: number) {
    if (index > -1) {
      this.dragactive = index;
    }
    ev.preventDefault();
  }
  active_Tab(index: number) {
    this.activeTab = index;
  }

  setAvatar(id: number) {
    this.photos.forEach((_, i) => {
      this.photos[i].isAvatar = false;
    });
    this.photoService.setPhotoAvatar(id).subscribe(_ => {
      const galleryphotoindex = this.photos.findIndex(it => it.id === id);
      this.photos[galleryphotoindex].isAvatar = true;
    });
  }
  removeFromGallery(id: number) {
    this.photoService.deleteGalleryPhoto(id).subscribe(_ => {
      const galleryphotoindex = this.photos.findIndex(it => it.id === id);
      this.photos.splice(galleryphotoindex, 1);
      this.rooms.forEach((item, i) => {
        const photoindex = item.photos.findIndex(it => it.id === id);
        if (photoindex > -1) {
          this.rooms[i].photos.splice(photoindex, 1);
        }
      });
    });
  }
  removeFromRoomGallery(index: number, id: number, roomid: number) {
    this.photoService.deleteRoomPhoto(id, roomid).subscribe(_ => this.rooms[this.activeTab].photos.splice(index, 1));
  }
  addRoomPhoto(id: number, src: string, index: number) {
    this.photoService.addRoomPhoto(id, this.rooms[index].id)
      .subscribe(_ => this.rooms[index].photos.push({id: id, isAvatar: false, name: src})
    );
    this.photos.forEach((item, ind) => {
      if (item.checked) {
        this.photos[ind].checked = false;
        this.photoService.addRoomPhoto(item.id, this.rooms[index].id)
          .subscribe(_ => this.rooms[index].photos.push({id: item.id, isAvatar: false, name: item.name})
          );
      }
    });
  }
  drag(ev) {
    ev.dataTransfer.setData('text', ev.target.attributes['data-src'].value);
    ev.dataTransfer.setData('id', ev.target.id);
  }

  drop(ev, index: number) {
    this.dragactive = -1;
    ev.preventDefault();
    // console.log(ev.dataTransfer.getData('text'));
    // console.log(ev.dataTransfer.getData('id'));
    this.addRoomPhoto(Number(ev.dataTransfer.getData('id')), ev.dataTransfer.getData('text'), index);
  }
}
