export class Photo {
	constructor(
	public id: number,
	public name: string,
	public isAvatar: boolean,
  public checked?: boolean
	){}
}
