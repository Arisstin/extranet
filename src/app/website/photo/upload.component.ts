import { Component, Output, EventEmitter } from '@angular/core';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { FileUploader } from 'ng2-file-upload';
import { RequestOptions } from '@angular/http';
import { HttpClient } from '@angular/common/http';

import { AuthService} from '../../auth/auth.service';
import { PhotoService } from './photo.service'
import { Globals } from '../../globals';
import {Observable} from 'rxjs/Rx';
import {BehaviorSubject} from 'rxjs/Rx';

import { Photo } from './photo';
import {Notify} from '../../widgets/notification/notification.classes';

@Component({
	selector:'file-uplode',
	templateUrl:'uplode.componet.html',
	styleUrls: ['uplode.component.css']
})
export class UploadComponent {
  @Output() update = new EventEmitter<boolean>();
	public uploader: FileUploader;
  public hasBaseDropZoneOver = false;
  public filePreviewPath: SafeUrl[];
  upd = false;
  notificate: any;
  successed = 0;
  errored = 0;

  constructor(
    private sanitizer: DomSanitizer,
    private http: HttpClient,
    private auth: AuthService,
    private photoService: PhotoService,
    private globals: Globals
    ) {
    const URL = this.globals.BACK_END + 'ru/extranet/photos/upload';
    this.uploader = new FileUploader({
      url: URL,
      allowedMimeType: ['image/jpeg', 'image/png' ],
      authToken: `Bearer ${this.auth.getToken()}`
    });
    this.filePreviewPath = [];
    this.uploader.onAfterAddingFile = (fileItem) => {
      this.uploader.uploadAll();
      // this.filePreviewPath.push(this.sanitizer.bypassSecurityTrustUrl(window.URL.createObjectURL(fileItem._file)));
      // console.log(this.filePreviewPath);
    };
    this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      this.photoService.addNewPhoto(response);
      this.successed++;
    };
    this.uploader.onCompleteAll = () => {
      console.log('complete');
      this.upd = !this.upd;
      this.update.emit(this.upd);
      if (this.errored === 0) { this.notificate = new Notify('post', 'success'); } else {
        if (this.successed > this.errored) {
          this.notificate = new Notify('post', 'warning');
        } else {
          this.notificate = new Notify('post', 'error');
        }
      }
      this.errored = 0;
      this.successed = 0;
    };
    this.uploader.onErrorItem = (() => {
      this.errored++;
    });
  }
  // removeUploaded(index: number) {
  //   this.filePreviewPath.splice(index, 1);
  // }
}
