import { Injectable }    from '@angular/core';
import {Observable} from 'rxjs/Rx';

import {BaseService} from '../../core/base.service';

import { RoomCategory } from './room-category';
import { RoomTitle } from './room-title';
import { RoomFacility } from './room-facility'
import { Room } from './room'

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class RoomService extends BaseService{

  getRoomCategory(): Observable<RoomCategory[]> {
    return this.get("room-categories");
  }

  getRoomTitles(categoryId): Observable<RoomTitle[]> {
  	let url = `room-titles/${categoryId}`;
  	return this.get(url);
  }

  getRoomFacilities(): Observable<RoomFacility[]> {
  	return this.get("room-facilities");
  }
  getBeds(): Observable<any> {
    return this.get('beds');
  }
  getRooms(): Observable<Room[]> {
    return this.get("rooms");
  }

  getRoom(id: number): Observable<Room> {
    let url = `rooms/${id}`;
    return this.get(url);
  }
  checkAliasName(data: string): Observable<any> {
    return this.post('rooms/check-uiqueness', {alias: data});
  }
  postRoom(room: Room): Observable<Room>{
    return this.post('rooms', room);
  }

  putRoom(id: number, room: Room): Observable<Room>{
    let url =  `rooms/${id}`;
    return this.put(url, room);
  }
  roomRemove(id: number): Observable<any> {
    return this.post('rooms/' + id + '/remove', {});
  }
}
