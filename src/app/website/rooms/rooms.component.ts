import { Component, OnInit } from '@angular/core';
import { Globals } from '../../globals';
import { RoomService } from './room.service';
import { Room } from './room';

@Component({
  selector: 'rooms',
  templateUrl: './rooms.component.html',
  styleUrls: ['./rooms.component.css']
})
export class RoomsComponent implements OnInit {
  rooms: Room[];
  emptyRooms: boolean;
  loaded = false;
  imageUrl: string;
  hotelId: string;
  removeId: number;
  constructor(private roomService: RoomService, private globals: Globals) {
    this.imageUrl = this.globals.IMAGE_URL;
    this.hotelId = localStorage.getItem('hotelId');
  }
  ngOnInit(): void {
    this.roomService.getRooms().subscribe(rooms => {
      this.rooms = rooms;
      this.loaded = true;
      }, err => {
      this.loaded = true;
      if (err.status === 404) {
        this.emptyRooms = true;
      }
    });
  }
  roomRemove(id: number) {
    this.roomService.roomRemove(id).subscribe(res => {
      const index = this.rooms.findIndex(x => x.id === id);
      if (index > -1) {
        this.rooms.splice(index, 1);
      }
    });
  }
}
