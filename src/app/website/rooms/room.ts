import { Photo } from '../photo/photo';

export class Room {
	id: number;
	categoryId: number;
	categoryType: string;
	titleId: number;
	aliase: string;
	guest: number;
	children: number;
	count: number;
	area: number;
	photos: Photo[];
	facilities: number[];
	beds: any[];
}
export class Bed {
  typeId: string;
  count: number;
  constructor(id) {
    this.typeId = id;
    this.count = 1;
  }
}
