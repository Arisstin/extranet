import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RoomService } from './room.service';
import { RoomCategory } from './room-category';
import {Bed, Room} from './room';
import { RoomTitle } from './room-title';
import { RoomFacility } from './room-facility';
import {Notify} from '../../widgets/notification/notification.classes';


@Component({
  selector: 'add-room',
  templateUrl: './add-room.component.html',
  styleUrls: ['./add-room.component.css']
})
export class AddRoomComponent implements OnInit {
  notificate: any;
  room: Room;
  aliasUniq = true;
  editMod: boolean;
  currentName: string;
  roomCategories: RoomCategory[];
  roomTitles: RoomTitle[];
  roomFacilities: RoomFacility[];
  id: number;
  beds: any[];
  constructor(
    private roomService: RoomService,
    private route: ActivatedRoute
    ) {}
    ngOnInit(): void {
    this.roomService.getRoomCategory()
      .subscribe(roomCategories => this.roomCategories = roomCategories);
    this.roomService.getRoomFacilities()
      .subscribe(roomFacilities => this.roomFacilities = roomFacilities, err => this.roomFacilities = []);
    this.roomService.getBeds()
      .subscribe(beds => this.beds = beds);
    this.route.params.subscribe( params => this.id = params.id);
    this.room = new Room();
    this.room.beds = [];
    this.room.facilities = [];
    if (this.id) {
      this.roomService.getRoom(this.id).subscribe(room => {
        this.room = room;
        if (!this.room.beds) { this.room.beds = []; }
        this.editMod = true;
        this.currentName = this.room.aliase;
        this.roomService.getRoomTitles(this.room.categoryId)
          .subscribe(roomTitles => this.roomTitles = roomTitles);
      });
    }
  }
  onbedChange(event, id: number): void {
    if (event.target.checked) {
      this.room.beds.push({
        typeId: id,
        count: 1
      });
    } else {
      const index = this.room.beds.findIndex(x => x.typeId === id);
      if (index > -1) {
        this.room.beds.splice(index, 1);
      }
    }
  }
  onBedCountChange(event, id: number) {
    const ind = this.room.beds.findIndex(x => x.typeId === id);
    this.room.beds[ind].count = Number(event.target.value);
  }
  countBeds(id: number) {
    const ind = this.room.beds.findIndex(x => x.typeId === id);
    return this.room.beds[ind].count;
  }
  initCheckboxBed(id: number): boolean {
    return this.room.beds.find(x => x.typeId === id);
  }
  getRoomName(id) {
    const ind = this.roomTitles.findIndex(x => x.id === Number(id));
    return this.roomTitles[ind].title;
  }
  findTransform() {
    return this.room.beds.find(x => x.typeId === 9);
  }
  getBedIndex(id) {
    return this.beds.findIndex(x => x.id === id);
  }
  onSelectRoomCategory(event): void {
    this.room.titleId = undefined;
    this.roomService.getRoomTitles(event)
      .subscribe(roomTitles => this.roomTitles = roomTitles);
  }
  bedImage(type: string) {
    switch (type) {
      case "BUNK" : { return 'bunk.svg'; }
      case "ARMBED" : { return 'chair.svg'; }
      default: {
        if (type[0] === 'S') {
          return type.includes('SOFA') ? 'sofa-1.svg' : 'bed-1.svg';
        } else {
          return type.includes('SOFA') ? 'sofa-2.svg' : 'bed-2.svg';
        }
      }
    }
  }
  onSubmit(): void {
    console.log(this.room);
    if (this.editMod) {
      this.roomService.putRoom(this.id, this.room).subscribe(
        res => this.notificate = new Notify('post', 'success'),
        err => this.notificate = new Notify('post', 'error'));
    } else {
      this.roomService.postRoom(this.room).subscribe(
        res => { this.notificate = new Notify('post', 'success'); this.editMod = true; this.id = res.id; },
        err => this.notificate = new Notify('post', 'error'));
    }
  }
  onFacilityChange(event, id: number): void {
    if (event.target.checked) {
      this.room.facilities.push(id);
    } else {
      const index = this.room.facilities.indexOf(id);

      if (index > -1) {
        this.room.facilities.splice(index, 1);
      }
    }
  }
  saveAvailable() {
    return !this.room.titleId || !this.room.categoryId || this.room.children < 0 ||
      !this.room.guest || this.room.guest < 1 ||
      !this.room.area || this.room.area <= 0 ||
      !this.room.count || this.room.count < 1;
  }
  initCheckbox(id: number): boolean {
    return this.room.facilities.indexOf(id) > -1;
  }

}
