import { Injectable }    from '@angular/core';
import {Observable} from 'rxjs/Rx';

import {BaseService} from '../../core/base.service';

import { Facility } from './facility'
import { HotelFacility } from './hotel-facility'

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class FacilityService extends BaseService{

 getFacilities():Observable<Facility[]>{
   return this.get('hotel/all-facilities');
 }

  getHotelFacilities():Observable<HotelFacility>{
   return  this.get('hotel/facilities');
 }

 postHotelFacility(hf:HotelFacility):Observable<HotelFacility>{
 	return this.post('hotel/facilities', hf);
 }

  putHotelFacility(hf:HotelFacility):Observable<HotelFacility>{
 	return this.post('hotel/facilities/update', hf);
 }
}
