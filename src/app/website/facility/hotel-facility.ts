import { Internet } from './internet';
import { Parking } from './parking';
import { Reception } from './reception';

export class HotelFacility{
	constructor(
		public internet:Internet,
		public parking: Parking,
		public reception: Reception,
		public facilities: number[]
		){

	}
}
