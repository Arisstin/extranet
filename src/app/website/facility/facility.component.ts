import { Component, OnInit } from '@angular/core';

import { FacilityService } from './facility.service';
import { Facility } from './facility';
import { HotelFacility } from './hotel-facility';

import { Internet } from './internet';
import { Parking } from './parking';
import { Reception } from './reception';
import {Notify} from '../../widgets/notification/notification.classes';
import * as moment from 'moment';



@Component({
  selector: 'facility',
  templateUrl: './facility.component.html',
  styleUrls: ['./facility.component.css']
})
export class FacilityComponent implements OnInit {
  facilities: Facility[];
  notificate: any;
  timeStartReception = new Date();
  timeEndReception = new Date();
  hf: HotelFacility;
  isNewRecord= true;
  constructor( private facilityService: FacilityService) {
    this.timeStartReception.setHours(6, 0, 0);
    this.timeEndReception.setHours(23, 0, 0);
    this.hf = new HotelFacility(
      new Internet(null, null, null),
      new Parking(null, null),
      new Reception(true, null, null),
      []
    );
  }
  ngOnInit() {
    this.facilityService.getFacilities()
      .subscribe(facilities => this.facilities = facilities, err => {
        this.facilities = [];
      });
    this.facilityService.getHotelFacilities()
      .subscribe(
        hf => {
          this.hf = hf;
          this.isNewRecord = false;
          console.log(this.hf.reception);
          if (hf.reception.start) {
            const timearr = this.hf.reception.start.split(':');
            this.timeStartReception.setHours(Number(timearr[0]), Number(timearr[1]), Number(timearr[2]));
          }
          if (hf.reception.end) {
            const timearr = this.hf.reception.end.split(':');
            this.timeEndReception.setHours(Number(timearr[0]), Number(timearr[1]), Number(timearr[2]));
          }
          },
        );
  }
  onFacilityChange( event, id: number): void {
    if (event.target.checked) {
      this.hf.facilities.push(id);
    } else {
      const index = this.hf.facilities.indexOf(id);
      if (index > -1) {
        this.hf.facilities.splice(index, 1);
      }
    }
  }
  onSubmit() {
    if (!this.hf.reception.isRoundClock) {
      this.hf.reception.start = moment(this.timeStartReception).format('HH:mm:ss');
      this.hf.reception.end = moment(this.timeEndReception).format('HH:mm:ss');
    } else {
      delete this.hf.reception.start;
      delete this.hf.reception.end;
    }
    if (this.isNewRecord) {
      this.facilityService.postHotelFacility(this.hf).subscribe(
        hf => { this.hf = hf; this.notificate = new Notify('post', 'success'); },
          err => this.notificate = new Notify('post', 'error')
      );
    } else {
      this.facilityService.putHotelFacility(this.hf).subscribe(
        hf => { this.hf = hf; this.notificate = new Notify('post', 'success'); },
          err => this.notificate = new Notify('post', 'error')
      );
    }
  }
  checkParking(event) {
    if (event !== 'PAID') {
      delete this.hf.parking.price;
    }
  }
  checkInternet(event) {
    if (event === 'NO') {
      delete this.hf.internet.accessPoint;
      delete this.hf.internet.type;
    }
  }
  checkReception(event) {}
  initCheckbox(id: number): boolean {
    return this.hf.facilities.indexOf(id) > -1;
  }
}
