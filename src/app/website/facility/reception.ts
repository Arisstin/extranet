export class Reception {
  constructor(
    public isRoundClock: boolean = true,
    public start: string,
    public end: string
  ) {}
}
