export class Facility{
	constructor(
	public id:number,	
	public type:string,
	public belongTo:string,
	public title:string
	){}
}