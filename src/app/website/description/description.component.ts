import { Component } from '@angular/core';
import {DescriptionSevice} from './description.sevice';
import {Descriptions} from './descriptions.classes';
import {Notify} from '../../widgets/notification/notification.classes';

@Component({
  selector: 'description',
  templateUrl: './description.component.html',
  styleUrls: ['./description.component.css']
})
export class DescriptionComponent {
  loaded = false;
  notificate: any;
  descriptions: Descriptions;
  isNewRecord = true;
  posted = false;
  text = 'Спасибо за помощь в описании Вашего объекта. Вскоре мы опубликуем описание на странице Вашего объекта.';
  constructor(private descriptionservices: DescriptionSevice) {
    this.loaded = true;
    this.descriptions = new Descriptions();
    this.descriptionservices.getDescriptions().subscribe(
      res => {
        this.isNewRecord = false;
        // this.loaded = true;
        // this.descriptions = res;
      },
        err => {
        // this.loaded = true;
        // if (err.status === 404) {
        //   this.descriptions = new Descriptions();
        // }
      });
  }
  clearing(key: string) {
    if (this.descriptions[key] === this.text) {
      this.descriptions[key] = '';
    }
  }
  check() {
    if (this.descriptions.rooms !== '') { this.descriptions.rooms = this.text; }
    if (this.descriptions.location !== '') { this.descriptions.location = this.text; }
    if (this.descriptions.nutrition !== '') { this.descriptions.nutrition = this.text; }
    if (this.descriptions.environment !== '') { this.descriptions.environment = this.text; }
  }
  onSubmit() {
    if (this.isNewRecord) {
      this.descriptionservices.createDescriptions(this.descriptions).subscribe(res => {
        this.notificate = new Notify('post', 'success');
        this.posted = true;
        this.check();
      }, err => {
        this.notificate = new Notify('post', 'error');
      });
    } else {
      this.descriptionservices.updateDescriptions(this.descriptions).subscribe(res => {
        this.notificate = new Notify('post', 'success');
        this.posted = true;
        this.check();
      }, err => {
        this.notificate = new Notify('post', 'error');
      });
    }
  }
}
