import { Injectable } from '@angular/core';
import {Observable} from 'rxjs/Rx';
import {BaseService} from '../../core/base.service';
import {Descriptions} from '../description/descriptions.classes';
@Injectable()
export class DescriptionSevice extends BaseService {
  getDescriptions(): Observable<Descriptions> {
    return this.get('descriptions');
  }
  createDescriptions(data: Descriptions): Observable<any> {
    return this.post('descriptions/create', data);
  }
  updateDescriptions(data: Descriptions): Observable<any> {
    return this.post('descriptions/update', data);
  }
}
