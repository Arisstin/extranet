import {Component, OnInit} from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { LoginService } from '../auth/login.service';
import {Router} from '@angular/router';
import {Hotel} from '../general/hotel/hotel';
import {HotelService} from '../general/hotel/hotel.service';
import { BsLocaleService } from 'ngx-bootstrap';
import {LocalizeRouterService} from 'localize-router';
import {Title} from '@angular/platform-browser';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'Extranet';
  hotel: Hotel;
  constructor(
    translate: TranslateService,
    private loginService: LoginService,
    private router: Router,
    private hotelservice: HotelService,
    private localeservice: BsLocaleService,
    private titles: Title
    ) {
    this.localeservice.use('ru');
    translate.setDefaultLang('ru');
    translate.use('ru');
    //this.localizeservice.changeLanguage('en')
    localStorage.removeItem('openlogin');
    }
    ngOnInit() {
      this.hotelservice.getHotel().subscribe(hotel => {
        localStorage.setItem('hotelId', hotel.id);
        if (localStorage.getItem('admin')) {
          this.titles.setTitle('Admin-' + hotel.title);
        }
        this.hotel = hotel; });
  }

  logout(): void {
    this.loginService.removeToken();
    localStorage.removeItem('admin');
    this.router.navigate( ['/ru/login']);
  }
}
