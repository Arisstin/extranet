import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HttpClient} from '@angular/common/http';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { Location  } from '@angular/common';

import { defineLocale, ruLocale, enGbLocale, BsDatepickerModule, TimepickerModule, TabsModule, ModalModule, TooltipModule,
  AlertModule, SortableModule, CarouselModule, PaginationModule } from 'ngx-bootstrap';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {TranslateModule, TranslateLoader} from '@ngx-translate/core';
import {TranslateHttpLoader } from '@ngx-translate/http-loader';
import {TranslateService} from '@ngx-translate/core';
import { FileDropDirective, FileSelectDirective, FileUploadModule } from 'ng2-file-upload';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { AppRoutingModule } from './rout/app-routing.module';
import { DataTablesModule } from 'angular-datatables';

import {LocalizeRouterModule, LocalizeRouterSettings, LocalizeParser, ManualParserLoader, LocalizeRouterService} from 'localize-router';

////
import { TokenInterceptor } from './auth/token.interceptor';
import { JwtInterceptor } from './auth/jwt.interceptor';
import { AppComponent } from './app/app.component';
import { HotelComponent } from './general/hotel/hotel.component';
import { AuthModalComponent } from './auth/auth-modal.component';
import { AuthModalFormComponent } from './auth/auth-modal-form.component';
import { WebSiteComponent } from './submenues/web-site.component';
import { GeneralInfoComponent } from './submenues/general-info.component';
import { RoomsComponent } from './website/rooms/rooms.component';
import { AddRoomComponent } from './website/rooms/add-room.component';
import { TariffMenueComponent } from './submenues/tariff-menue.component';
import { AddTariffComponent } from './tariffs/add-tariff.component';
import { UploadComponent } from './website/photo/upload.component';
import { PhotoComponent } from './website/photo/photo.component';
import { GalleryComponent } from './website/photo/gallery.component';
import { FacilityComponent } from './website/facility/facility.component';
import { ReviewComponent } from './tariffs/review/review.component';
import { ReviewTableComponent } from './tariffs/review/review-table.component';
import { TariffPriceComponent } from './tariffs/review/tariff-price.component';
import { DailyPriceComponent } from './tariffs/review/daily-price.component';
import { RoomsCountComponent } from './tariffs/review/rooms-count.component';
import { TariffDaysComponent } from './tariffs/review/tariff-days.component';
import { DescriptionComponent } from './website/description/description.component';
import { SpecialOfferComponent } from './submenues/special-offer.component';
import { BaseDiscountComponent } from './special-offers/base/base-discount.component';
import { LastMinuteComponent } from './special-offers/last-minute/last-minute.component';
import { MinStayComponent } from './special-offers/min-stay/min-stay.component';
import {ContactsComponent} from './general/contacts/contacts.component';
import {LandmarksComponent} from './general/landmarks/landmarks.component';
import {PaymentComponent} from './general/payment/payment.component';
import {TaxesComponent} from './general/taxes/taxes.component';
import {PoliciesComponent} from './general/policies/policies.component';
// import { Ng2SliderComponent } from 'ng2-slider-component/ng2-slider.component';
// import { SlideAbleDirective } from 'ng2-slideable-directive/slideable.directive';
// import { Ng2StyledDirective } from 'ng2-styled-directive/ng2-styled.directive';
// import { BaseService } from  './core/base.service';
import { HotelService } from './general/hotel/hotel.service';
import { AuthService } from './auth/auth.service';
import { LoginService} from './auth/login.service';
import { RoomService } from './website/rooms/room.service';
import { TariffService } from './tariffs/tariff.service';
import { PhotoService } from './website/photo/photo.service';
import { FacilityService } from './website/facility/facility.service';
import { ReviewService } from './tariffs/review/review.service';
import { EventService } from './tariffs/review/event.service';
import { GeneralServices } from './general/general.services';
import { DescriptionSevice } from './website/description/description.sevice';
import { FinanceServices } from './finance/finance.services';
import { Globals } from './globals';
import { SpecialOffersServices } from './special-offers/special-offers.services';
import { NouisliderModule } from 'ng2-nouislider';

import {routes} from './rout/routes';
import { BookingComponent } from './booking/booking.component';
import { BookingServices } from './booking/booking.services';
import { AuthPageComponent } from './auth/auth-page/auth-page.component';
import { BookingTableComponent } from './booking/booking-table/booking-table.component';
import { LoadingComponent } from './widgets/loading/loading.component';
import { NotificationComponent } from './widgets/notification/notification.component';
import { LoadingErrorComponent } from './widgets/loading-error/loading-error.component';
import { FinanceComponent } from './finance/finance.component';
import { BookingConfirmComponent } from './booking/booking-confirm/booking-confirm.component';
import { TextMaskModule } from 'angular2-text-mask';

defineLocale('ru', ruLocale);
defineLocale('en', enGbLocale);
export function HttpLoaderFactory(http: HttpClient) {
    return new TranslateHttpLoader(http);
}
export function LocalizeRouterFactory(
  translate: TranslateService,
  location: Location,
  settings: LocalizeRouterSettings) {
  return  new ManualParserLoader(translate, location, settings, ['en', 'ru', 'ua'], 'en');
}

@NgModule({
  declarations: [
    AppComponent,
    HotelComponent,
    AuthModalComponent,
    AuthModalFormComponent,
    WebSiteComponent,
    GeneralInfoComponent,
    RoomsComponent,
    AddRoomComponent,
    TariffMenueComponent,
    AddTariffComponent,
    UploadComponent,
    PhotoComponent,
    GalleryComponent,
    DescriptionComponent,
    FacilityComponent,
    // FileDropDirective,
    // FileSelectDirective,

    ReviewComponent,
    ReviewTableComponent,
    TariffPriceComponent,
    DailyPriceComponent,
    RoomsCountComponent,
    TariffDaysComponent,
    SpecialOfferComponent,
    BaseDiscountComponent,
    LastMinuteComponent,
    MinStayComponent,
    TaxesComponent,
    PoliciesComponent,
    ContactsComponent,
    LandmarksComponent,
    PaymentComponent,
    BookingComponent,
    AuthPageComponent,
    BookingTableComponent,
    LoadingComponent,
    NotificationComponent,
    LoadingErrorComponent,
    FinanceComponent,
    BookingConfirmComponent
    // SlideAbleDirective,
    // Ng2StyledDirective,
    // Ng2SliderComponent
  ],
  imports: [
    TextMaskModule,
    SimpleNotificationsModule.forRoot(),
    FileUploadModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    DataTablesModule,
    NouisliderModule,
    PaginationModule.forRoot(),
    TabsModule.forRoot(),
    BsDatepickerModule.forRoot(),
    ModalModule.forRoot(),
    CarouselModule.forRoot(),
    TimepickerModule.forRoot(),
    TabsModule.forRoot(),
    SortableModule.forRoot(),
    TooltipModule.forRoot(),
    AlertModule.forRoot(),
    LocalizeRouterModule.forRoot(routes, {
     parser: {
        provide: LocalizeParser,
        useFactory: LocalizeRouterFactory,
        deps: [TranslateService, Location, LocalizeRouterSettings]
      }
    }),
    TranslateModule.forRoot({
      loader: {
          provide: TranslateLoader,
          useFactory: HttpLoaderFactory,
          deps: [HttpClient]
      }
    })
  ],
  entryComponents: [AuthModalFormComponent, TariffPriceComponent, DailyPriceComponent, RoomsCountComponent, TariffDaysComponent],
  providers: [
    Globals,
    GeneralServices,
  AuthService,
  LoginService,
  TranslateService,
  RoomService,
  TariffService,
  PhotoService,
  FacilityService,
    DescriptionSevice,
    FinanceServices,
  ReviewService,
  EventService,
    BookingServices,
    SpecialOffersServices,
  {
    provide: HTTP_INTERCEPTORS,
    useClass: TokenInterceptor,
    multi: true
  },
  {
    provide: HTTP_INTERCEPTORS,
    useClass: JwtInterceptor,
    multi: true
  },
  HotelService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
