export class Notify {
  mod: string;
  result: string;
  constructor(mod, result) {
    this.mod = mod;
    this.result = result;
  }
}
