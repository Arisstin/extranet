import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import { NotificationsService } from 'angular2-notifications';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.css']
})
export class NotificationComponent implements OnInit, OnChanges {
  @Input() option: any;
  public options = {
    position: ['bottom', 'left'],
    timeOut: 1500
  };
  constructor(private notificationservice: NotificationsService) {
  }
  ngOnChanges(changes: SimpleChanges) {
    console.log(changes);
    if (changes.option.currentValue !== undefined) {
      if (changes.option.currentValue.result === 'success') {
        this.notificationservice.success(
          '',
          'Данные успешно сохранено'
        );
      }
      if (changes.option.currentValue.result === 'error') {
        this.notificationservice.error(
          '',
          'Не удалось сохранить данные'
        );
      }
      if (changes.option.currentValue.result === 'warning') {
        this.notificationservice.success(
          '',
          'Данные частично сохранены'
        );
      }
    }
  }

  ngOnInit() {
    console.log('notify');
  }
}
